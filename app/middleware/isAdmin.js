export default ({ $auth, redirect }) => {
	const userIsAdmin = $auth.loggedIn && $auth.user.admin
	if(!userIsAdmin) {
		redirect('/')	
	}
}
