export const actions = {
	async nuxtServerInit({ dispatch }, context) {
		await dispatch('checkout/load', context)
	}
}