export const state = () => ({
	campaign: null,
	tickets: [],
	order: null
})

export const mutations = {
	campaign(state, value) {
		state.campaign = value
	},
	tickets(state, value) {
		state.tickets = value
	},
	order(state, value) {
		state.order = value
	}
}

export const actions = {
	async load({ commit, rootGetters }, { app }) {
		const loggedIn = rootGetters['sessions/loggedIn']
		if(!loggedIn) return
		const cookie = this.$cookies.get('checkout')
		if(cookie === undefined) return
		const { campaignId, tickets, orderId } = cookie
		let response = await this.$api.campaigns.get(campaignId)
		const campaign = response.data
		commit('campaign', campaign)
		commit('tickets', tickets)
		if(orderId !== undefined) {
			response = await this.$api.orders.get(orderId)
			const order = response.data
			commit('order', order)
		}
	},

	new({ commit }, { campaign, tickets }) {
		tickets = [...tickets]
		tickets.sort((a, b) => a - b)
		commit('campaign', campaign)
		commit('tickets', tickets)
		this.$cookies.set('checkout', {
			campaignId: campaign.id,
			tickets
		})
	},

	setOrder({ commit }, order) {
		commit('order', order)
		const cookie = this.$cookies.get('checkout')
		this.$cookies.set('checkout', {
			orderId: order.id,
			...cookie
		})
	}
}
