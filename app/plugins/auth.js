import HTTPStatus from 'http-status'

export default function({ app, $axios, redirect }) {
	$axios.onResponseError(error => {
		const $auth = app.$auth
		const status = parseInt(error.response && error.response.status)
		if(status === HTTPStatus.UNAUTHORIZED && error.config.url !== $auth.strategy.options.endpoints.login.url) {
			if($auth.loggedIn) {
				$auth.reset()
			}
			$auth.redirect('sessionExpired')
			return Promise.resolve(error.response)
		}

		return Promise.reject(error)
	})
}
