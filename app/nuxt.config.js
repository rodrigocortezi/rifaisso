const isDev = process.env.NODE_ENV === 'development'
const baseURL = 'http://api:3000'
const browserBaseURL = isDev ? 'http://localhost:3000' : `https://${process.env.API_HOST}`

export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: 'RIFAISSO! - Crie rifas online',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Crie quantas rifas quiser, totalmente grátis para qualquer lugar do Brasil.' },
      { hid: 'og:site_name', property: 'og:site_name', content: 'RifaIsso' },
      { hid: 'og:type', property: 'og:type', content: 'website' },
      { hid: 'twitter:site', name: 'twitter:site', content: '@rifaisso' },
      {
        hid: 'twitter:card',
        name: 'twitter:card',
        content: 'summary_large_image'
      },
      {
        hid: 'og:image',
        property: 'og:image',
        content: 'https://rifaisso.com.br/rifaisso-card.jpg'
      },
      {
        hid: 'og:image:secure_url',
        property: 'og:image:secure_url',
        content: 'https://rifaisso.com.br/rifaisso-card.jpg'
      },
      {
        hid: 'og:image:alt',
        property: 'og:image:alt',
        content: 'RifaIsso - Crie rifas online'
      },
      {
        hid: 'twitter:image',
        name: 'twitter:image',
        content: 'https://rifaisso.com.br/rifaisso-card.jpg'
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#6D4AFB' },
  /*
  ** Global CSS
  */
  css: [
    '@/assets/scss/custom.scss'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/dinero',
    '~/plugins/mask',
    '~/plugins/validate',
    '~/plugins/auth'
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/moment'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    'cookie-universal-nuxt',
    'nuxt-buefy',
    // Doc: https://axios.nuxtjs.org/usage
    'yamlful-nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/auth'
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    baseURL,
    browserBaseURL,
    progress: false
  },
  auth: {
    localStorage: false,
    redirect: {
      login: '/user/login',
      sessionExpired: '/user/login?expired=true',
      home: '/',
      logout: '/'
    },
    strategies: {
      local: {
        endpoints: {
          login: { url: '/auth/token', method: 'post', propertyName: 'access_token' },
          logout: false,
          user: { url: '/promoter/me', method: 'get', propertyName: false }
        }
      }
    }
  },
  /*
  ** Moment.js configuration
  */
  moment: {
    defaultLocale: 'pt-br',
    locales: ['pt-br'],
    defaultTimezone: 'Brasilia'
  },
  /*
  ** Build configuration
  */
  build: {
    transpile: [
      "vee-validate/dist/rules"
    ],
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
      config.node = {
        fs: 'empty'
      }
    }
  },
  watchers: {
    chokidar: {
      usePolling: true,
      useFsEvents: false
    },
    webpack: {
      aggregateTimeout: 300,
      poll: 1000
    }
  }
}
