import Base from './base.js'
import moment from 'moment'

const nextTargetDate = () => {
	const targetDays = [3, 6]
	const today = moment().day()

	let date = moment()

	if(!targetDays.includes(today)) {
		if(today < targetDays[0]) {
			date.day(targetDays[0])
		} else {
			date.day(targetDays[1])
		}
	}

	return date.toDate()
}

export default class Campaign extends Base {
	static model = 'campaign'
	static fields() {
		return {
			id: null,
			updatable: true,
			title: '',
			purpose: '',
			draw_date: nextTargetDate(),
			goal: null,
			category: null,
			videos: [],
			photos: {
				association: true,
				fields: {
					id: null,
					url: '',
					image: null,
					name: null
				}
			},
			raffle: {
				association: true,
				fields: {
					id: null,
					quantity: null,
					price_cents: null
				}
			},
			prizes: {
				association: true,
				fields: {
					id: null,
					title: ''
				}
			}
		}
	}

	routes() {
		const endpoints = {
			true: {
				save: '/admin/campaigns',
				fetch: '/admin/campaigns'
			},
			false: {
				save: 'promoter/campaigns',
				fetch: 'campaigns'
			}
		}

		return endpoints[Boolean(this.isAdmin)]
	}
}
