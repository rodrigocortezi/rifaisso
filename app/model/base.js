import Vue from 'vue'
import { objectToFormData } from 'object-to-formdata'
import pluralize from 'pluralize'
import { pick } from 'lodash'
import path from 'path'

const capitalize = s => {
	if (typeof s !== 'string') return ''
  return s.charAt(0).toUpperCase() + s.slice(1)
}

const uncapitalize = s => {
	if (typeof s !== 'string') return ''
  return s.charAt(0).toLowerCase() + s.slice(1)
}

export default class Base {
	constructor(attributes = {}) {
		this._data = {}
		this.saving = false
		this.loading = false

		this.clear()

		// Creates build and remove functions for associations
		const fields = this.constructor.fields()
		for(let [property, options] of Object.entries(fields)) {
			if(options !== null && typeof options === 'object' && options.association) {
				let buildFunction
				let removeFunction
				let functionName
				if(pluralize.isSingular(property)) {
					buildFunction = attributes => {
						const fields = Object.keys(options.fields)
						let filtered = pick(attributes, fields)
						const newObject = Object.assign({}, options.fields, filtered)
						this._data[property] = newObject
					}
				} else {
					buildFunction = attributes => {
						const fields = Object.keys(options.fields)
						let filtered = pick(attributes, fields)
						const newObject = Object.assign({}, options.fields, filtered)
						this._data[property].push(newObject)
					}
					removeFunction = index => {
						const obj = this[property][index]
						if(obj.id !== null) {
							Vue.set(obj, '_destroy', true)
						} else {
							const rawIndex = this._data[property].indexOf(obj)
							this._data[property].splice(rawIndex, 1)
						}
					}
					functionName = `remove${capitalize(pluralize.singular(property))}`
					this[functionName] = removeFunction
				}
				functionName = `build${capitalize(pluralize.singular(property))}`
				this[functionName] = buildFunction
			}
		}

		this.set(attributes)

		// Set getters and setters to attributes
		for(let key in this.constructor.fields()) {
			Object.defineProperty(this, key, {
				get: () => this.get(key),
				set: (value) => this.set({ [key]: value })
			})
		}
	}

	static fields() {
		return {}
	}

	routes() {
		return {}
	}

	getFetchURL(id=null) {
		const routes = this.routes()
		if(routes['fetch'] === undefined) {
			return ''
		}
		let param = id || ''
		param = '' + param
		return path.join(routes['fetch'], param)
	}

	getSaveURL(id=null) {
		const routes = this.routes()
		if(routes['save'] === undefined) {
			return ''
		}
		let param = id || ''
		param = '' + param
		return path.join(routes['save'], param)
	}

	clear() {
		this._data = {}
		const fields = this.constructor.fields()
		for(let [property, value] of Object.entries(fields)) {
			if(value !== null && typeof value === 'object' && value.constructor.name !== 'Date') {
				const defaultValue = pluralize.isSingular(property) ? null : []
				this._data[property] = defaultValue
			} else {
				this._data[property] = value
			}
		}
	}

	set(attributes) {
		const fields = Object.keys(this.constructor.fields())
		let filtered = pick(attributes, fields)
		Object.assign(this._data, filtered)
	}

	get(attribute) {
		const fields = this.constructor.fields()
		if(fields[attribute] !== null && typeof fields[attribute] === 'object' && fields[attribute].association && pluralize.isPlural(attribute)) {
			return this._data[attribute].filter(e => !e._destroy)
		} else {
			return this._data[attribute]
		}
	}

	created() {
		return this._data.hasOwnProperty('id') && this._data.id !== null
	}

	getSaveData() {
		let data = this._data
		if(typeof this.dataTransform === 'function') {
			data = this.dataTransform(this._data)
		}
		data = this.constructor.parseServerData(data)
		const model = this.constructor.model
		const formData = objectToFormData({ [model]: data }, { indices: true })
		return formData
	}

	static sendRequest(options) {
		return Vue.prototype.$nuxt.$axios.request(options)
	}

	sendSaveRequest() {
		let config
		if(this.created()) {
			const id = this._data.id
			config = {
				url: this.getSaveURL(id),
				method: 'put',
				data: this.getSaveData()
			}
		} else {
			config = {
				url: this.getSaveURL(),
				method: 'post',
				data: this.getSaveData()
			}
		}

		return Base.sendRequest(config)
	}

	sendFetchRequest(id) {
		let config = {
			url: this.getFetchURL(id),
			method: 'get'
		}

		return Base.sendRequest(config)
	}

	async save() {
		try {
			this.saving = true
			const response = await this.sendSaveRequest()
			let data = this.constructor.parseClientData(response.data)
			this.set(data)
		} finally {
			this.saving = false
		}
	}

	async fetch(id) {
		let data = null
		await this.sendFetchRequest(id).then((response) => {
			data = this.constructor.parseClientData(response.data)
		})

		return data
	}

	static parseClientData(data) {
		let parsed = {}
		for(let [key, value] of Object.entries(data)) {
			key = key.replace('_attributes', '')
			parsed[key] = value
		}

		const fields = Object.keys(this.fields())
		parsed = pick(parsed, fields)

		return parsed
	}

	static parseServerData(data) {
		const fields = this.fields()
		const parsed = {}
		for(let [key, value] of Object.entries(data)) {
			const options = fields[key]
			let property = key
			if(options !== null && typeof options === 'object' && options.association) {
				property += '_attributes'
			}
			parsed[property] = value
		}

		return parsed
	}
}
