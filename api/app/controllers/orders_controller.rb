class OrdersController < ApplicationController
  before_action :doorkeeper_authorize!, except: :create
  load_and_authorize_resource except: :create
  include ErrorHandling

  # GET /orders
  def index
    @pagy, @orders = pagy(Order.accessible_by(current_ability, :read))
    render json: OrderBlueprint.render(@orders)
  end

  # GET /orders/count
  def count
    render json: { count: Order.accessible_by(current_ability, :read).count }
  end

  def show
    render json: OrderBlueprint.render(@order, view: :payment)
  end

  # POST /orders
  def create
    result = CreateOrder.call(
      user_params,
      new_order_params[:campaign_id],
      new_order_params[:tickets],
      new_order_params[:payment]
    )
    if result.success?
      @order = result.order
      render json: OrderBlueprint.render(@order, view: :payment), status: :created
    else
      render_api_error(result.error)
    end
  end

  private

    def user_params
      new_order_params[:user]
    end

    def new_order_params
      params.require(:order).permit(
        :campaign_id,
        tickets: [],
        user: [
          :name,
          :email,
          :document_number,
          :birthdate,
          :phone,
          address_attributes: [
            :id,
            :street,
            :number,
            :complement,
            :district,
            :zipcode,
            :city,
            :state
          ]
        ],
        payment: [
          :instrument,
          credit_card: [
            :fullname,
            :document_number,
            :birthdate,
            :phone,
            :hash
          ]
        ]
      )
    end

    def current_ability
      @current_ability ||= OrderAbility.new(current_resource_owner)
    end
end
