class PrizeReceiptsController < ApplicationController
  include ErrorHandling

	def create
		@prize = Prize.find(params[:id])
		if @prize.received_at.present?
			render_api_error({ code: :prize_already_received })
			return
		end

		if @prize.update(received_at: DateTime.current)
			campaign_prizes = @prize.campaign.prizes
			receipts = campaign_prizes.map { |p| p.received_at.present? }
			if receipts.all?
				ReleaseCampaignEscrowJob.perform_later(@prize.campaign_id)
			end
			render json: @prize
		else
			render json: @prize.errors, status: :unprocessable_entity
		end
	end
end
