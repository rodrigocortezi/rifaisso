class ArticlesController < ApplicationController

  # GET /articles
  def index
    @articles = ArticleBlueprint.render(Article.all)
    render json: @articles
  end

  # GET /articles/1
  def show
    @article = ArticleBlueprint.render(Article.find(params[:id]))
    render json: @article
  end

end
