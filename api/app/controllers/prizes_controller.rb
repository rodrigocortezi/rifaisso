class PrizesController < ApplicationController
  include ErrorHandling

	def index
		@user = User.find_by(prize_params)
		unless @user.present?
			render_api_error({ code: :user_not_found })
			return
		end
		@prizes = @user.prizes.not_received
		render json: @prizes
	end

	private

		def prize_params
			params.permit(:document_number)
		end
end
