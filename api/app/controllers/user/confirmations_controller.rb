class User::ConfirmationsController < Devise::ConfirmationsController
	private
		def after_confirmation_path_for(resource_name, resource)
			host = Rails.env.development? ? 'http://localhost:5000' : "https://#{ENV.fetch('APP_HOST')}"
			host + '/user/login?account_confirmation_success=true'
		end
end
