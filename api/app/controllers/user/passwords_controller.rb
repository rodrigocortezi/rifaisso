class User::PasswordsController < Devise::PasswordsController
  def create
    user = User.find_by_email(resource_params["email"])
    if user.present? && user.has_no_password?
      self.resource = User.to_adapter.get!(user.id)
      resource.errors.add(:email, :not_found)
      respond_with(resource)
    else
      super
    end
  end
end
