class User::RegistrationsController < Devise::RegistrationsController
	# Prevent session parameter from being passed
	# Unpermitted parameter: registration
	wrap_parameters format: []
  before_action :doorkeeper_authorize!, only: :update
  skip_before_action :authenticate_scope!, only: :update

	def create
    resource = User.find_by_email(sign_up_params[:email])
    if resource.present? && !resource.confirmed? && resource.has_no_password?
      # Change name
      resource.update(sign_up_params)

      # Resets password
      new_password = new_password_confirmation = sign_up_params[:password]
      resource.skip_password_change_notification!
      resource.reset_password(new_password, new_password_confirmation)

      # Sends confirmation instructions
      resource.send_confirmation_instructions

      render json: resource
    else
      super
    end
	end

	def update
		resource = current_resource_owner
		resource_updated = update_resource(resource, account_update_params)
		if resource_updated
			render json: resource
		else
			respond_with resource
		end
	end

	protected

		def build_resource(hash)
			return unless @resource.nil?
			super(hash)
		end

		def update_resource(resource, params)
			if params.include?(:password)
				resource.update_with_password(params)
			else
				resource.update_without_password(params)
			end
		end

	private

		def account_update_params
			params.permit(
				:email,
				:current_password,
				:password,
				:name,
				:document_number,
				:birthdate,
				:phone,
				photo_attributes: [
					:id,
					:image
				],
				address_attributes: [
					:id,
					:street,
					:number,
					:complement,
					:district,
					:city,
					:state,
					:zipcode
				]
			).reject { |_, v| v.blank? }
		end
end
