class ContactsController < ApplicationController
  # POST /contacts
  def create
    ContactMailer.with(contact: contact_params).support_email.deliver_later
    head :no_content
  end

  private

    def contact_params
      params.require(:contact).permit(:name, :email, :subject, :message)
    end
end
