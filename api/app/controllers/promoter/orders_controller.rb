class Promoter::OrdersController < ApplicationController
	before_action :doorkeeper_authorize!
	load_and_authorize_resource

	def index
		@orders = Order.accessible_by(current_ability, :read).paid
		unless filter_params.empty?
			@orders = @orders.created_between(filter_params[:created_at_after], filter_params[:created_at_before])
		end
		render json: OrderBlueprint.render(@orders)
	end

	def show
		render json: OrderBlueprint.render(@order, view: :payment)
	end

	private

		def filter_params
			params.permit(:created_at_before, :created_at_after)
		end

		def current_ability
			@current_ability ||= Promoter::OrderAbility.new(current_resource_owner)
		end
end
