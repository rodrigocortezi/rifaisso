class Promoter::CampaignsController < ApplicationController
	before_action :doorkeeper_authorize!
	load_and_authorize_resource

	# GET /campaigns
	def index
		@pagy, @campaigns = pagy(Campaign.accessible_by(current_ability, :read))
		render json: CampaignBlueprint.render(@campaigns, view: :manage, image_size: :medium, thumbnail: true)
	end

	# GET /campaign/1
	def show
		render json: CampaignBlueprint.render(@campaign, view: :promoter)
	end

	# POST /campaigns
	def create
		@user = current_resource_owner
		if @user.moip_id.nil?
			account = Moip.api.accounts.create({
				transparent_account: true,
				email: {
					address: @user.email
				},
				person: {
					name: @user.first_name,
					last_name: @user.last_name,
					tax_document: {
						type: 'CPF',
						number: @user.document_number
					},
					birth_date: @user.birthdate.strftime('%Y-%m-%d'),
					phone: {
						country_code: '55',
						area_code: @user.phone_area_code,
						number: @user.phone_number
					},
					address: {
						street: @user.address.street,
						street_number: @user.address.number,
						complement: @user.address.complement,
						district: @user.address.district,
						zip_code: @user.address.zipcode,
						city: @user.address.city,
						state: @user.address.state,
						country: 'BRA'
					}
				},
				type: 'MERCHANT'
			})
			@user.update(moip_id: account.id, moip_access_token: account.access_token)
		end
    if @campaign.save
      render json: CampaignBlueprint.render(@campaign), status: :created
    else
      render json: @campaign.errors, status: :unprocessable_entity
    end
	end

	# PATCH/PUT /campaigns/1
	def update
		if @campaign.update(campaign_params)
      render json: CampaignBlueprint.render(@campaign)
		else
      render json: @campaign.errors, status: :unprocessable_entity
		end
	end

	# DELETE /campaigns/1
	def destroy
		unless @campaign.active?
			render 'Campaign not active', status: :unprocessable_entity
			return
		end

		unless @campaign.sold?
			@campaign.destroy
		else
			CancelSoldCampaignJob.perform_later(@campaign.id)
		end
	end

	private

		def campaign_params
			unless params[:campaign][:videos].blank? || params[:campaign][:videos].is_a?(Array)
				params[:campaign][:videos] = params[:campaign][:videos].values
			end
			params.require(:campaign)
				.permit(
					:id,
					:title,
					:purpose,
					:draw_date,
					:goal,
					:category,
					videos: [],
					photos_attributes: [
						:id,
						:_destroy,
						:image
					],
					raffle_attributes: [
						:id,
						:quantity,
						:price_cents
					],
					prizes_attributes: [
						:id,
						:_destroy,
						:title,
						:position
					]
				)
		end

		def current_ability
			@current_ability ||= Promoter::CampaignAbility.new(current_resource_owner)
		end
end
