class Promoter::FinancesController < ApplicationController
	before_action :doorkeeper_authorize!
	load_and_authorize_resource :finance, :through => :current_resource_owner, singleton: true

	def show
		render json: @finance
	end

	private

		def current_ability
			@current_ability ||= Promoter::FinanceAbility.new(current_resource_owner)
		end
end
