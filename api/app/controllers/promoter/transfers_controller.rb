class Promoter::TransfersController < ApplicationController
	before_action :doorkeeper_authorize!
	load_and_authorize_resource

  include ErrorHandling

	def create
		balance = current_resource_owner.finance.balance
		if @transfer.total > balance
			render_api_error({ code: :insufficient_funds })
			return
		end

		@transfer.bank_account = current_resource_owner.finance.bank_account

    if @transfer.save
    	unavailable = current_resource_owner.finance.unavailable + @transfer.total
    	current_resource_owner.finance.update(unavailable_cents: unavailable.cents)
      render json: @transfer, status: :created
    else
      render json: @transfer.errors, status: :unprocessable_entity
    end
	end

	private

		def transfer_params
			params.require(:transfer).permit(:total_cents)
		end

		def current_ability
			@current_ability ||= Promoter::TransferAbility.new(current_resource_owner)
		end
end
