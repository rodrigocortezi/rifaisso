class Promoter::UsersController < ApplicationController
	before_action :doorkeeper_authorize!

	def show
		render json: current_resource_owner
	end
end
