class Promoter::BankAccountsController < ApplicationController
	before_action :doorkeeper_authorize!
	load_resource :finance, :through => :current_resource_owner, singleton: true
	load_and_authorize_resource :through => :finance, singleton: true

	def show
		render json: @bank_account	
	end

	def create
		if @bank_account.save
			render json: @bank_account, status: :created, location: @bank_account
		else
			render json: @bank_account.errors, status: :unprocessable_entity
		end
	end

	def update
		@bank_account.assign_attributes(bank_account_params)

		if @bank_account.transfers.exists?
			@bank_account = @bank_account.dup
			@bank_account.finance = @finance
		end

		if @bank_account.save
			render json: @bank_account
		else
			render json: @bank_account.errors, status: :unprocessable_entity
		end
	end

	private

		def bank_account_params
			params.require(:bank_account)
				.permit(
					:legal_name,
					:document_type,
					:document_number,
					:bank_code,
					:account_type,
					:agency_number,
					:agency_check_number,
					:account_number,
					:account_check_number
				)
		end

		def current_ability
			@current_ability ||= Promoter::BankAccountAbility.new(current_resource_owner)
		end
end
