module ErrorHandling
	def render_api_error(error)
		details = I18n.t("api.errors.#{error[:code]}")
		if error[:code] == :invalid_resource
			details[:message] = error[:data].errors.full_messages
		end
		e = details.slice(:code, :title, :message)
		render json: { error: e }, status: details[:status]
	end
end