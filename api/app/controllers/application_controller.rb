class ApplicationController < ActionController::API
	respond_to :json
	before_action :configure_permitted_parameters, if: :devise_controller?
	include Pagy::Backend

	rescue_from CanCan::AccessDenied do |exception|
		render json: { message: exception.message }, status: :forbidden
	end

  protected

	def current_resource_owner
		User.find(doorkeeper_token.resource_owner_id) if doorkeeper_token
	end

	def configure_permitted_parameters
		devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
	end
end
