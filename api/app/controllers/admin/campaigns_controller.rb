class Admin::CampaignsController < ApplicationController
	before_action :doorkeeper_authorize!

	# GET /admin/campaigns
	def index
		@pagy, @campaigns = pagy(Campaign.all)
		render json: CampaignBlueprint.render(@campaigns, view: :manage)
	end

	# GET /admin/campaigns/:id
	def show
		@campaign = Campaign.find(params[:id])
		render json: CampaignBlueprint.render(@campaign, view: :manage)
	end

	# GET /admin/campaigns/count
	def count
		render json: { count: Campaign.count }
	end

	# PATCH/PUT /admin/campaigns/1
	def update
		@campaign = Campaign.find(params[:id])
		if @campaign.update(campaign_params)
      render json: CampaignBlueprint.render(@campaign)
		else
      render json: @campaign.errors, status: :unprocessable_entity
		end
	end

	# DELETE /admin/campaigns/1
	def destroy
		@campaign = Campaign.find(params[:id])
		unless @campaign.active?
			render 'Campaign not active', status: :unprocessable_entity
			return
		end

		unless @campaign.sold?
			@campaign.destroy
		else
			CancelSoldCampaignJob.perform_later(@campaign.id)
		end
	end

	private

		def campaign_params
			unless params[:campaign][:videos].blank? || params[:campaign][:videos].is_a?(Array)
				params[:campaign][:videos] = params[:campaign][:videos].values
			end
			params.require(:campaign)
				.permit(
					:id,
					:title,
					:purpose,
					:draw_date,
					:goal,
					:category,
					videos: [],
					photos_attributes: [
						:id,
						:_destroy,
						:image
					],
					raffle_attributes: [
						:id,
						:quantity,
						:price_cents
					],
					prizes_attributes: [
						:id,
						:_destroy,
						:title,
						:position
					]
				)
		end
end
