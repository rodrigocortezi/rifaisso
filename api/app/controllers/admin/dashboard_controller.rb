class Admin::DashboardController < ApplicationController
	before_action :doorkeeper_authorize!

	def index
		@lastMonthActive = Campaign.where(status: [:active, :waiting_receipt])
			.or(Campaign.where(draw_date: 28.days.ago..DateTime.now, status: :finished)).count()
		@lastMonthDraws = Campaign.where(draw_date: 28.days.ago..DateTime.now).count()
		@lastMonthSoldTickets = Ticket.active.joins(:raffle).joins(:order).where('orders.created_at BETWEEN ? AND ?', 28.days.ago, DateTime.now).count()
		@lastMonthAdminRevenue = (Money.new(Order.paid.where(created_at: 28.days.ago..DateTime.now).sum(:total_cents)) * 0.1).cents

		render json: {
			lastMonthActive: @lastMonthActive,
			lastMonthDraws: @lastMonthDraws,
			lastMonthSoldTickets: @lastMonthSoldTickets,
			lastMonthAdminRevenue: @lastMonthAdminRevenue
		}
	end
end
