class Admin::TransfersController < ApplicationController
	before_action :doorkeeper_authorize!

	def index
		in_analysis = Transfer.in_analysis.order(created_at: :desc)
		others = Transfer.not_in_analysis.order(created_at: :desc)
		@transfers = in_analysis + others
		render json: TransferBlueprint.render(@transfers, view: :admin)
	end

	def approve
		@transfer = Transfer.find(params[:transfer_id])
		@finance = @transfer.user.finance

		@moip_transfer = create_moip_transfer @transfer

		UpdateBalanceJob.perform_later(@transfer.user.id)
		unavailable_balance = @finance.unavailable - @transfer.total
		@finance.update(unavailable_cents: unavailable_balance.cents)

		unless @moip_transfer.respond_to?(:id)
			@transfer.update(status: :failed)
			render json: TransferBlueprint.render(@transfer, view: :admin)
			return
		end

		if @transfer.update(status: :requested, moip_id: @moip_transfer.id)
			render json: TransferBlueprint.render(@transfer, view: :admin)
		else
			render json: @transfer.errors, status: :unprocessable_entity
		end
	end

	def deny
		@transfer = Transfer.find(params[:transfer_id])
		@finance = @transfer.user.finance

		unavailable_balance = @finance.unavailable - @transfer.total
		@finance.update(unavailable_cents: unavailable_balance.cents)

		if @transfer.update(status: :not_approved)
			render json: TransferBlueprint.render(@transfer, view: :admin)
		else
			render json: @transfer.errors, status: :unprocessable_entity
		end
	end

	private

		def create_moip_transfer(data)
			Moip.api.transfer.create(
				amount: data.total_cents,
				transferInstrument: {
					method: "BANK_ACCOUNT",
					bankAccount: {
						type: data.bank_account.account_type,
						bankNumber: data.bank_account.bank_code,
						agencyNumber: data.bank_account.agency_number,
						agencyCheckNumber: data.bank_account.agency_check_number,
						accountNumber: data.bank_account.account_number,
						accountCheckNumber: data.bank_account.account_check_number,
						holder: {
							fullname: data.bank_account.legal_name,
							taxDocument: {
								type: data.bank_account.document_type,
								number: data.bank_account.document_number
							}
						}
					}
				}
			)
		end
end
