class Admin::OrdersController < ApplicationController
	before_action :doorkeeper_authorize!

	# GET /admin/orders
	def index
		if params[:campaign_id]
			@campaign = Campaign.find(params[:campaign_id])
			@pagy, @orders = pagy(@campaign.orders)
			render json: OrderBlueprint.render(@orders, view: :payment)
		else
			@pagy, @orders = pagy(Order.all)
			render json: OrderBlueprint.render(@orders, view: :payment)
		end
	end

	# GET /admin/orders/count
	def count
		if params[:campaign_id]
			@campaign = Campaign.find(params[:campaign_id])
			render json: { count: @campaign.orders.count }
		else
			render json: { count: Order.count }
		end
	end

	# GET /admin/orders/:id
	def show
		@order = Order.find(params[:id])
		render json: OrderBlueprint.render(@order, view: :payment)
	end
end
