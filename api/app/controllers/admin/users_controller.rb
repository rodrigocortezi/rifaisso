class Admin::UsersController < ApplicationController
	before_action :doorkeeper_authorize!

	# GET /admin/users
	def index
		@pagy, @users = pagy(User.all)	
		render json: UserBlueprint.render(@users)
	end

	# GET /admin/users/count
	def count
		render json: { count: User.count }
	end
end