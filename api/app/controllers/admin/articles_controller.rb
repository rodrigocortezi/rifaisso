class Admin::ArticlesController < ApplicationController
	before_action :doorkeeper_authorize!

	def create
		@article = Article.new(article_params)
		if @article.save
			render json: ArticleBlueprint.render(@article), status: :created
		else
			render json: @article.errors, status: :unprocessable_entity
		end
	end

	def update
		@article = Article.find(params[:id])
		if @article.update(article_params)
			render json: ArticleBlueprint.render(@article)
		else
			render json: @article.errors, status: :unprocessable_entity
		end
	end

	def destroy
		@article = Article.find(params[:id])
		@article.destroy
	end

	private

		def article_params
			params.require(:article).permit(
				:title,
				:text,
				photo_attributes: [
					:id,
					:image
				]
			)
		end

end
