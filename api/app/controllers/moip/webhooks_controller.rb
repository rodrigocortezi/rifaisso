class Moip::WebhooksController < ApplicationController
	before_action :authenticate

	def create
		Moip::Webhooks.listen(request) do |hook|
			hook.on(:payment, :pre_authorized) do |resource|
				payment = Payment.find_by(moip_id: resource.id)
				order = payment.order
				if order.waiting?
					Moip.api.payment.capture(payment.moip_id)
				else
					Moip.api.payment.void(payment.moip_id)
				end
			end

			hook.on(:payment, :authorized) do |resource|
				payment = Payment.find_by(moip_id: resource.id)
				unless payment.nil? || payment.settled?
					order = payment.order
					PaymentManagement.new(payment: payment).settle_payment
					OrderMailer.with(order: order).payment_confirmed_email.deliver_later
				end
			end

			hook.on(:payment, :cancelled) do |resource|
				payment = Payment.find_by(moip_id: resource.id)
				unless payment.nil? || payment.cancelled?
					order = payment.order
					PaymentManagement.new(payment: payment).cancel_payment
					OrderMailer.with(order: order).payment_cancelled_email.deliver_later
				end
			end

			hook.on(:payment, :refunded) do |resource|
				payment = Payment.find_by(moip_id: resource.id)
				unless payment.nil? || payment.refunded?
					order = payment.order
					PaymentManagement.new(payment: payment).refund_payment
					OrderMailer.with(order: order).payment_refunded_email.deliver_later
					publisher = order.campaign.publisher
					UpdateBalanceJob.perform_later(publisher.id)
				end
			end

			hook.on(:payment, :reversed) do |resource|
				payment = Payment.find_by(moip_id: resource.id)
				unless payment.nil? || payment.reversed?
					order = payment.order
					PaymentManagement.new(payment: payment).reverse_payment
					OrderMailer.with(order: order).payment_reversed_email.deliver_later
					publisher = order.campaign.publisher
					UpdateBalanceJob.perform_later(publisher.id)
				end
			end

			hook.on(:escrow, :released) do |resource|
				payment = Payment.find_by(escrow_id: resource.id)
				unless payment.nil?
					publisher = payment.order.campaign.publisher
					UpdateBalanceJob.perform_later(publisher.id)
				end
			end

			hook.on(:transfer, :completed) do |resource|
				transfer = Transfer.find_by(moip_id: resource.id)		
				unless transfer.nil?
					transfer.update(status: :completed)
				end
			end

			hook.on(:transfer, :failed) do
				transfer = Transfer.find_by(moip_id: resource.id)		
				unless transfer.nil?
					transfer.update(status: :failed)
				end
			end
		end

		head :no_content
	end

	private

		def authenticate
			ActiveSupport::SecurityUtils.secure_compare(request.headers["Authorization"], Moip.notification_token)
		end
end
