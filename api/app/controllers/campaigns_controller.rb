class CampaignsController < ApplicationController
  # GET /campaigns
  def index
    @campaigns = Campaign.featured
    @campaigns = Campaign.featured_categories if params[:featured_categories].present?
    @campaigns = Campaign.most_recent if params[:most_recent].present?
    @campaigns = @campaigns.by_category(params[:category]) if params[:category].present?
    @campaigns = Campaign.search_title(params[:q]).sellable if params[:q].present?
    render json: CampaignBlueprint.render(@campaigns, image_size: :medium, thumbnail: true)
  end

  # GET /campaigns/1
  def show
    @campaign = CampaignBlueprint.render(
      Campaign.find(params[:id]),
      view: :tickets,
      image_size: :large
    )
    render json: @campaign
  end
end
