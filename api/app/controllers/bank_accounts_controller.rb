class BankAccountsController < ApplicationController
  before_action :set_bank_account, only: [:show, :update, :destroy]

  # GET /bank_account
  def show
    render json: @bank_account
  end

  # POST /bank_account
  def create
    @bank_account = BankAccount.new(bank_account_params)

    if @bank_account.save
      render json: @bank_account, status: :created, location: @bank_account
    else
      render json: @bank_account.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /bank_account
  def update
    if @bank_account.update(bank_account_params)
      render json: @bank_account
    else
      render json: @bank_account.errors, status: :unprocessable_entity
    end
  end

  # DELETE /bank_account
  def destroy
    @bank_account.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bank_account
      @bank_account = current_resource_owner.bank_account
    end

    # Only allow a trusted parameter "white list" through.
    def bank_account_params
      params.require(:bank_account).permit(:legal_name, :document_type, :document_number, :bank_code, :agency_number, :agency_check_number, :account_number, :account_check_number)
    end
end
