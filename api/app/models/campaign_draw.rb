class CampaignDraw
	class InvalidDrawDate < StandardError; end
	class CampaignGoalNotBeaten < StandardError; end

	attr_reader :campaign

	def initialize(campaign, lottery_draw=nil)
		@campaign = campaign
		@lottery_draw = lottery_draw
	end

	def init
		validate_draw

		tickets = campaign.tickets.active
		winnig_tickets = []
		draw_size = [campaign.prizes.size, 5].min

		lottery_draw.slice(0, draw_size).each do |number|
			if number >= raffle_quantity
				number = number % (10 ** (number.to_s.length - 1))
				number = 1 + rand(number)
				winning = tickets.order(updated_at: :desc).limit(number).last
			else
				winning = tickets.find_by(number: number)
				if winning.nil?
					number = 1 + rand(number)
					winning = tickets.order(updated_at: :desc).limit(number).last
				end
			end

			winnig_tickets.push(winning)
		end

		winnig_tickets
	end

	private

		def lottery_draw
			numbers = @lottery_draw || (0..10**5-1).to_a.shuffle
			mask = Math.log10(raffle_quantity).ceil
			numbers.pop(5).map do |n|
				n.to_s.rjust(5, '0')[5-mask, mask].to_i
			end
		end

		def raffle_quantity
			@raffle_quantity ||= campaign.raffle.quantity
		end

		def validate_draw
			if campaign.draw_date.nil? || campaign.draw_date.to_date != Date.current
				raise InvalidDrawDate, 'Campaign draw date is invalid.'
			elsif !campaign.goal_beaten?
				raise CampaignGoalNotBeaten, 'Campaign goal not been beaten.'
			end
		end
end
