# == Schema Information
#
# Table name: tickets
#
#  id         :bigint           not null, primary key
#  number     :integer          not null
#  status     :enum             not null
#  raffle_id  :bigint           not null
#  order_id   :bigint           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Ticket < ApplicationRecord
	enum status: {
		available: 'available',
		reserved: 'reserved',
		active: 'active',
		cancelled: 'cancelled'
	}

	validates :number, numericality: { greater_than_or_equal_to: 0 }
	validates :status, presence: true
	validate :unique_reserved_or_active_number

	belongs_to :raffle
	belongs_to :order
	has_one :owner, through: :order, source: :user
	has_one :prize

	after_initialize :set_defaults

	def self.bought
		self.not_cancelled
	end

	private

		def unique_reserved_or_active_number
			if (reserved? || active?) && raffle.tickets.where(number: number, status: [:reserved, :active]).where.not(id: self.id).exists?
				errors.add(:number, :taken, value: number)
			end
		end
		def set_defaults
			self.status ||= :reserved
		end
end
