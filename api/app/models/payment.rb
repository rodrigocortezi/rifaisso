# == Schema Information
#
# Table name: payments
#
#  id           			:bigint           not null, primary key
#  total_cents  			:integer          not null
#  fees_cents   			:integer          not null
#  liquid_cents 			:integer          not null
#  refund_cents 			:integer          default(0), not null
#  card_brand   			:string
#  card_number  			:string
#  boleto_link  			:string
#  boleto_line_code		:string
#  instrument   			:enum             not null
#  status       			:enum             not null
#  order_id     			:bigint           not null
#  created_at   			:datetime         not null
#  updated_at   			:datetime         not null
#
class Payment < ApplicationRecord
	enum status: {
		processing: 'processing',
		settled: 'settled',
		cancelled: 'cancelled',
		refunded: 'refunded',
		reversed: 'reversed'
	}

	enum instrument: {
		boleto: 'boleto',
		credit_card: 'credit_card'
	}, _suffix: true

	validates :total_cents, numericality: { greater_than: 0 }
	validates :fees_cents, numericality: { greater_than_or_equal_to: 0 }
	validates :status, presence: true
	validates :instrument, presence: true
	validates :card_brand, presence: true, if: :credit_card_instrument?
	validates :card_number, presence: true, if: :credit_card_instrument?
	validates :card_brand, absence: true, if: :boleto_instrument?
	validates :card_number, absence: true, if: :boleto_instrument?

	belongs_to :order
end
