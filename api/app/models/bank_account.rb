# == Schema Information
#
# Table name: bank_accounts
#
#  id                   :bigint           not null, primary key
#  legal_name           :string           not null
#  document_type        :string           not null
#  document_number      :string           not null
#  bank_code            :string           not null
#  account_type         :string           not null
#  agency_number        :string           not null
#  agency_check_number  :string
#  account_number       :string           not null
#  account_check_number :string           not null
#  moip_id              :string
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
class BankAccount < ApplicationRecord
	validates :legal_name, presence: true
	validates :document_type, presence: true
	validates :document_number, presence: true
	validates :document_number, format: { with: /\A\d*\Z/ }
	validates :bank_code, presence: true
	validates :account_type, presence: true
	validates :agency_number, presence: true
	validates :account_check_number, presence: true
	validates :account_number, presence: true

	has_one :finance
	has_many :transfers
end
