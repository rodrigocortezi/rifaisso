# == Schema Information
#
# Table name: orders
#
#  id                :bigint           not null, primary key
#  number            :string           not null
#  total_cents       :integer          not null
#  service_fee       :decimal          not null
#  status            :enum             not null
#  user_id           :bigint           not null
#  campaign_id       :bigint           not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
class Order < ApplicationRecord
	enum status: {
		waiting: 'waiting',
		paid: 'paid',
		not_paid: 'not_paid',
		reverted: 'reverted'
	}

	validates :total_cents, numericality: { greater_than: 0 }
	validates :total_fee_cents, numericality: { greater_than_or_equal_to: 0 }
	validates :status, presence: true

	belongs_to :user
	belongs_to :campaign
	has_one :payment
	has_many :tickets
	has_many :prizes, through: :tickets

	after_initialize :set_defaults

	before_save :update_total
	before_create :generate_number

	monetize :total_cents
	monetize :subtotal_cents
	monetize :total_fee_cents

	scope :created_between, -> (start_date, end_date) { where(created_at: start_date..end_date) }

	def total_cents
		(campaign.raffle.price * tickets.length).cents
	end

	def subtotal_cents
		(total - total_fee).cents
	end

	def total_fee_cents
		(service_fee * total).cents
	end

	def tickets_quantity
		tickets.count
	end

	private

		def set_defaults
			self.status ||= :waiting
			self.service_fee ||= Business::ServiceFee
		end

		def update_total
			self.total_cents = total_cents
		end

		def generate_number
			ascii = Array(?A..?Z)
			loop do
				c = ascii.sample(5)
				n = rand(10 ** 3).to_s.rjust(3, '0').split('')
				self.number = c[0] + c[1] + n[0] + c[2] + c[3] + n[1] + c[4] + n[2]
				break unless Order.exists?(number: self.number)
			end
		end
end
