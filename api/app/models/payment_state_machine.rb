class PaymentStateMachine
	include States

	state :processing
	state :settled
	state :cancelled
	state :refunded
	state :reversed

	transition from: :processing, to: [:settled, :cancelled]
	transition from: :settled,    to: [:refunded, :reversed]
end
