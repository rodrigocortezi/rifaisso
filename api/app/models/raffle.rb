# == Schema Information
#
# Table name: raffles
#
#  id          :bigint           not null, primary key
#  price_cents :integer          not null
#  quantity    :integer          not null
#  campaign_id :bigint           not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Raffle < ApplicationRecord
	belongs_to :campaign
	has_many :tickets

	validates :price_cents, numericality: { greater_than: 0 }
	validates :quantity, numericality: { greater_than: 0 }

	monetize :price_cents
end
