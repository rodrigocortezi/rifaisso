# frozen_string_literal: true

# == Schema Information
#
# Table name: photos
#
#  id                     :bigint           not null, primary key
#  record_id              :bigint           not null
#  record_type            :string           not null
#  image_data             :text             not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
class Photo < ActiveRecord::Base
	include ImageUploader::Attachment(:image)
	belongs_to :imageable, polymorphic: true

	before_save :generate_derivatives

	private

		def generate_derivatives
			self.image_derivatives! if self.image_changed?
		end
end
