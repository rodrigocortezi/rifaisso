class OrderStateMachine
	include States

	state :waiting
	state :not_paid
	state :paid
	state :reverted

	transition from: :waiting, to: [:not_paid, :paid]
	transition from: :paid,    to: :reverted
end
