class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :confirmable, :recoverable, :validatable

  validates :email, uniqueness: true
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :first_name, presence: true
  validates :last_name, presence: true

  has_one :finance, autosave: true
  has_many :transfers
  has_one :address
  has_many :campaigns, foreign_key: 'publisher_id'
  has_many :orders
  has_many :prizes, through: :orders
  has_one :photo, as: :imageable, dependent: :destroy

  accepts_nested_attributes_for :address
  accepts_nested_attributes_for :photo

  has_person_name

  attribute :phone, :string

  before_save do
    normalize_attributes
  end

  before_save :assign_finance

  def phone
    [phone_area_code, phone_number].join('')
  end

  def phone=(value)
    raw = value.gsub(/[^\d]/, '')
    self.phone_area_code = raw[0, 2]
    self.phone_number = raw[2..]
  end

  def has_complete_registration?
    %w(
      encrypted_password
      confirmed_at
      email
      first_name
      last_name
      document_number
      birthdate
      phone_area_code
      phone_number
      address
    ).all? { |attribute| send(attribute).present? }
  end

  def as_json(options={})
    json = super(options).merge({
      name: self.name,
      familiar_name: self.name.familiar
    })
    if self.address.present?
      json.merge!({
        address_attributes: self.address.as_json
      })
    end
    if self.photo.present?
      json.merge!({
        profile: {
          id: self.photo.id,
          url: self.photo.image_url(:medium)
        }
      })
    end

    return json
  end

  def has_no_password?
    self.encrypted_password.blank?
  end

  protected

    def password_required?
      confirmed? ? super : false
    end

  private

    def assign_finance
      self.finance ||= build_finance
    end

    def normalize_attributes
      self.document_number = document_number.gsub(/[^\d]/, '') unless document_number.nil?
    end
end
