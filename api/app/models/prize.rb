# == Schema Information
#
# Table name: prizes
#
#  id          :bigint           not null, primary key
#  title       :string           not null
#  position    :integer          not null
#  campaign_id :bigint           not null
#  ticket_id   :bigint
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  received_at :datetime
#
class Prize < ApplicationRecord
	validates :title, presence: true

	belongs_to :campaign
	belongs_to :ticket, optional: true
	acts_as_list scope: :campaign

	scope :not_received, -> {
		where(received_at: nil)	
	}
end
