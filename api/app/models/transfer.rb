# == Schema Information
#
# Table name: transfers
#
#  id                 :bigint           not null, primary key
#  total_cents        :integer          not null
#  fee_cents          :integer          default(0), not null
#  status             :enum             not null
#  bank_account_id    :bigint           not null
#  user_id            :bigint           not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
class Transfer < ApplicationRecord
	enum status: {
		in_analysis: 'in_analysis',
		requested: 'requested',
		completed: 'completed',
		failed: 'failed',
		not_approved: 'not_approved'
	}

	validates :total_cents, numericality: { greater_than: 0 }
	validates :fee_cents, numericality: { greater_than_or_equal_to: 0 }
	validates :status, presence: true

	belongs_to :bank_account
	belongs_to :user

	after_initialize :set_defaults

	monetize :total_cents

	private

		def set_defaults
			self.status ||= :in_analysis
		end
end
