class PaymentManagement
	def initialize(order: nil, payment: nil)
		if order.nil? && payment.nil?
			raise ArgumentError
		end
		@order = order
		@payment = payment
	end

	def settle_payment
		ActiveRecord::Base.transaction do
			order_state_machine.transition_to(:paid)
			payment_state_machine.transition_to(:settled)
			tickets.each do |t|
				TicketStateMachine.new(t).transition_to(:active)
			end
		end
	end

	def cancel_payment
		ActiveRecord::Base.transaction do
			order_state_machine.transition_to(:not_paid)
			payment_state_machine.transition_to(:cancelled)
			tickets.each do |t|
				TicketStateMachine.new(t).transition_to(:cancelled)
			end
		end
	end

	def refund_payment
		ActiveRecord::Base.transaction do
			order_state_machine.transition_to(:reverted)
			payment_state_machine.transition_to(:refunded)
			tickets.each do |t|
				TicketStateMachine.new(t).transition_to(:cancelled)
			end
		end
	end

	def reverse_payment
		ActiveRecord::Base.transaction do
			order_state_machine.transition_to(:reverted)
			payment_state_machine.transition_to(:reversed)
			tickets.each do |t|
				TicketStateMachine.new(t).transition_to(:cancelled)
			end
		end
	end

	private

		def order
			@order ||= payment.order
		end

		def payment
			@payment ||= order.payment
		end

		def tickets
			@tickets ||= @order.tickets
		end

		def order_state_machine
			@order_state_machine ||= OrderStateMachine.new(order)
		end

		def payment_state_machine
			@payment_state_machine ||= PaymentStateMachine.new(payment)
		end
end