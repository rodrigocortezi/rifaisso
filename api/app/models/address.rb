# == Schema Information
#
# Table name: addresses
#
#  id         :bigint           not null, primary key
#  street     :string           not null
#  number     :string           not null
#  complement :string
#  district   :string           not null
#  city       :string           not null
#  state      :string           not null
#  zipcode    :string           not null
#  user_id    :bigint           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Address < ApplicationRecord
	validates :street, presence: true
	validates :number, presence: true
	validates :district, presence: true
	validates :city, presence: true
	validates :state, presence: true
	validates :zipcode, presence: true

	belongs_to :user

	before_save do
		self.zipcode = zipcode.gsub(/[^\d]/, '')
	end
end
