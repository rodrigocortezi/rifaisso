# == Schema Information
#
# Table name: campaigns
#
#  id           :bigint           not null, primary key
#  title        :string           not null
#  purpose      :text             not null
#  draw_date    :datetime         not null
#  goal         :decimal(, )      not null
#  status       :enum             not null
#  category     :enum             not null
#  videos       :array[:string]   not null
#  publisher_id :bigint           not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
class Campaign < ApplicationRecord
	include PgSearch::Model
	pg_search_scope :search_title,
    :against => [:title],
    :using => [:tsearch]

	enum status: {
		active: 'active',
		cancelled: 'cancelled',
		waiting_receipt: 'waiting_receipt',
		finished: 'finished'
	}
	enum category: {
		vehicles: 'vehicles',
		electronics: 'electronics',
		accessories: 'accessories',
		cash: 'cash',
		travels: 'travels',
		others: 'others'
	}, _suffix: true
	validates :title, presence: true
	validates :purpose, presence: true, length: { maximum: 1500 }
	validates :draw_date, presence: true
	validates :goal, inclusion: 0.5..1
	validates :raffle, presence: true
	validate :draw_date_cannot_be_in_the_past, on: :create
	validates :status, presence: true
	validates :category, presence: true

	belongs_to :publisher, class_name: :User
	has_one :raffle, autosave: true, dependent: :destroy
	has_many :prizes, autosave: true, dependent: :destroy
	has_many :tickets, through: :raffle
	has_many :orders
	has_many :payments, through: :orders
	has_many :photos, as: :imageable, dependent: :destroy

	accepts_nested_attributes_for :raffle
	accepts_nested_attributes_for :prizes, allow_destroy: true
	accepts_nested_attributes_for :photos, allow_destroy: true

	after_initialize :set_defaults

	scope :sellable, -> {
		merge(self.active).
		where('draw_date > ?', DateTime.now + 30.minutes)
	}

	scope :featured, -> {
		merge(self.sellable).
		left_outer_joins(:tickets).
		group(:id).
		order('count(tickets.id) desc')
	}

	scope :most_recent, -> {
		merge(self.sellable).
		order(created_at: :desc)
	}

	scope :by_category, -> (category) { where(category: category) }

	scope :draw_today, -> { where('draw_date::date = ?', Date.current) }

	def self.featured_categories
		featured_campaigns = []
		Campaign.categories.each_value do |category|
			campaigns = Campaign.featured.where(category: category).limit(3)
			featured_campaigns += campaigns
		end
		return featured_campaigns
	end

	def winners
		User.joins(orders: { tickets: :prize }).where('prizes.campaign_id = ?', self.id).distinct
	end

	def winning_tickets
		tickets.where(id: prizes.map(&:ticket_id))
	end

	def bought_tickets
		tickets.bought
	end

	def revenue_cents
		orders.paid.collect(&:subtotal_cents).sum
	end

	def admin_revenue_cents
		orders.paid.collect(&:total_fee_cents).sum
	end

	def draw_date=(value)
		super(value)
		date = draw_date.change(hour: 19)
		super(date)
	end

	def sellable?
		(active? && (draw_date > (DateTime.now + 30.minutes)))
	end

	def sold?
		orders.where(status: [:waiting, :paid]).exists?
	end

	def goal_beaten?
		(tickets.active.count.to_f / raffle.quantity) >= self.goal
	end

	private

	def draw_date_cannot_be_in_the_past
		if draw_date.present? && draw_date < Date.today
			errors.add(:draw_date, 'cannot be in the past')
		end
	end

	def set_defaults
		self.status ||= :active
		self.category ||= :others
	end
end
