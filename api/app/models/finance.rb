# == Schema Information
#
# Table name: finances
#
#  id                    :bigint           not null, primary key
#  balance_cents         :integer          default(0), not null
#  unavailble_cents      :integer          default(0)
#  bank_account_id       :bigint
#  user_id               :bigint           not null
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#
class Finance < ApplicationRecord
	belongs_to :bank_account, optional: true
	belongs_to :user

	validates :balance_cents, numericality: { greater_than_or_equal_to: 0 }

	after_initialize :set_defaults

	monetize :balance_cents
	monetize :unavailable_cents
	monetize :available_balance_cents

	def available_balance_cents
		(balance - unavailable).cents
	end

	def as_json(options={})
		super(options).merge({
			available_balance_cents: available_balance_cents
		})
	end

	private

		def set_defaults
			self.balance ||= 0
		end
end
