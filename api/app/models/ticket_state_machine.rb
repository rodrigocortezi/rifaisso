class TicketStateMachine
	include States

	state :available
	state :reserved
	state :active
	state :cancelled

	transition from: :available, to: [:reserved, :active]
	transition from: :reserved,  to: [:active, :cancelled]
	transition from: :active,    to: :cancelled
end