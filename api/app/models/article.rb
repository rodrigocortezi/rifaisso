class Article < ApplicationRecord
	validates :title, presence: true
	validates :text, presence: true
	has_one :photo, as: :imageable, dependent: :destroy

	accepts_nested_attributes_for :photo, allow_destroy: true
end
