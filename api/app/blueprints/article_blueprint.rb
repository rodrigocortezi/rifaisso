class ArticleBlueprint < Blueprinter::Base
	identifier :id

	fields :title, :text, :created_at
	field :photo do |article|
		article.photo&.image_url(:large)
	end
end