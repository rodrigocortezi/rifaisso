class AddressBlueprint < Blueprinter::Base
	identifier :id
	
	fields :street, :number, :complement, :district, :city, :state, :zipcode
end
