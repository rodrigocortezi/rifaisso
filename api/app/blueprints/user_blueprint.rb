class UserBlueprint < Blueprinter::Base
	identifier :id

	fields :email, :first_name, :last_name, :name, :birthdate, :document_number, :phone_area_code, :phone_number
	field :signed_up do |user|
		!user.has_no_password?
	end

	association :address, blueprint: AddressBlueprint, name: :address_attributes
	field :familiar_name do |user|
		user.name.familiar
	end

	field :avatar do |user|
		user.photo&.image_url(:small)
	end

	view :publisher do
		field :location do |user|
			address = user.address
			"#{address.city}/#{address.state}"
		end
	end
end
