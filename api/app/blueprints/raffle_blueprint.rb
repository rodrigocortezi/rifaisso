class RaffleBlueprint < Blueprinter::Base
	identifier :id
	
	fields :price_cents, :quantity
end
