class TransferBlueprint < Blueprinter::Base
	identifier :id

	fields :total_cents, :fee_cents, :status

	view :admin do
		field :bank_account do |transfer|
			transfer.bank_account.as_json
		end

		association :user, blueprint: UserBlueprint
	end

end
