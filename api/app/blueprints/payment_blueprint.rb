class PaymentBlueprint < Blueprinter::Base
	identifier :id

	fields :card_brand, :card_number, :instrument, :boleto_link, :boleto_line_code, :created_at
end
