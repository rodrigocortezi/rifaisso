class CampaignBlueprint < Blueprinter::Base
	identifier :id

	fields :title, :purpose, :draw_date, :goal, :category, :videos, :status
	field :videos, default: []

	field :photos do |campaign, options|
		size = options.fetch(:image_size, {})
		count = options[:thumbnail] ? 1 : campaign.photos.length
		campaign.photos.slice(0, count).map do |photo|
			{
				id: photo.id,
				name: photo.image.original_filename,
				url: photo.image_url(size)
			}
		end
	end

	field :tickets_quantity do |campaign|
		campaign.tickets.active.count
	end

	field :sellable do |campaign|
		campaign.sellable?
	end

	field :updatable do |campaign|
		!campaign.orders.where(status: [:waiting, :paid]).exists?
	end

	association :raffle, blueprint: RaffleBlueprint
	association :prizes, blueprint: PrizeBlueprint
	association :publisher, blueprint: UserBlueprint, view: :publisher
	association :winning_tickets, view: :owner, blueprint: TicketBlueprint

	view :tickets do
		association :bought_tickets, name: :tickets, view: :owner, blueprint: TicketBlueprint
	end

	view :promoter do
		fields :revenue_cents
		field :sales_quantity do |campaign|
			{
				total: campaign.orders.paid.count,
				cc: campaign.payments.credit_card_instrument.count,
				boleto: campaign.payments.boleto_instrument.count
			}
		end
	end

	view :manage do
		fields :status, :revenue_cents, :admin_revenue_cents
		field :sales_quantity do |campaign|
			{
				total: campaign.orders.paid.count,
				cc: campaign.payments.credit_card_instrument.count,
				boleto: campaign.payments.boleto_instrument.count
			}
		end
	end
end
