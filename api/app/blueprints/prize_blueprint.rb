class PrizeBlueprint < Blueprinter::Base
	identifier :id

	fields :title

  association :ticket, blueprint: TicketBlueprint, view: :owner
end
