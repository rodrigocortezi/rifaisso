class TicketBlueprint < Blueprinter::Base
	fields :number, :status

	view :owner do
		field :owner do |ticket|
			{
				name: ticket.owner.name,
				phone: ticket.owner.phone
			}
		end
	end
end