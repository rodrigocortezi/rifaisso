class OrderBlueprint < Blueprinter::Base
	identifier :id

	fields :number, :tickets_quantity, :subtotal_cents, :total_cents, :status, :created_at


	association :user, blueprint: UserBlueprint
	association :campaign, blueprint: CampaignBlueprint
	association :tickets, blueprint: TicketBlueprint

	view :payment do
		association :payment, blueprint: PaymentBlueprint
	end
end
