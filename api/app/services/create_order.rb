class CreateOrder < ApplicationService
	attr_reader :order

	def initialize(user, campaign, tickets, payment)
		@user = user
		@campaign = campaign
		@tickets = tickets
		@payment = payment
	end

	def call
		begin
			ActiveRecord::Base.transaction do
				validate_campaign
				validate_payment_instrument
				validate_tickets
				user = load_user
				@order = create_order(user)
				payment = create_payment(@order)
			end
			set_cancel_job
			OrderMailer.with(order: @order).created_email.deliver_later
		rescue PaymentInstrumentNotAvailable
			OpenStruct.new(success?: false, error: { code: :payment_instrument_not_available })
		rescue InvalidCampaign
			OpenStruct.new(success?: false, error: { code: :campaign_not_sellable })
		rescue TicketNotAvailable
			OpenStruct.new(success?: false, error: { code: :ticket_not_available })
		rescue PaymentError => e
			OpenStruct.new(success?: false, error: { code: e.error_code })
		rescue MoipError
			OpenStruct.new(success?: false, error: { code: :moip_response_error })
		rescue ActiveRecord::RecordInvalid => e
			OpenStruct.new(success?: false, error: { code: :invalid_resource, data: e.record })
		else
			OpenStruct.new(success?: true, order: @order)
		end
	end

	private

		def validate_campaign
			campaign = Campaign.find(@campaign)
			raise InvalidCampaign, 'Campaign not sellable' unless campaign.sellable?
		end

		def validate_payment_instrument
			campaign = Campaign.find(@campaign)			
			if @payment[:instrument] == 'boleto' && (campaign.draw_date < (DateTime.now + 7.days))
				raise PaymentInstrumentNotAvailable
			end
		end

		def validate_tickets
			campaign = Campaign.find(@campaign)
			@tickets.each do |t_number|
				ticket = campaign.raffle.tickets.build(number: t_number).tap(&:valid?)
				next if !ticket.errors.added?(:number, :taken, value: t_number)
				raise TicketNotAvailable.new
			end
		end

    def load_user
      user = User.find_by_email(@user[:email]) || User.new(@user)
      if user.persisted?
      	user.update!(@user)
      else
      	user.skip_confirmation_notification!
        user.save!
      end
      user
    end

		def build_order(user)
			campaign = Campaign.find(@campaign)
			order = Order.new(user_id: user.id, campaign_id: campaign.id)
			@tickets.each do |t_number|
				order.tickets.build(raffle_id: campaign.raffle.id, number: t_number)
			end
			order
		end

		def moip_api(entity, method_name, *args)
			resource = Moip.api.send(entity).send(method_name, *args)
			if resource.respond_to?(:errors)
				# TODO: Adicionar erro ao log de erros
				msg = resource.errors.collect(&:description).join('. ')
				raise MoipError.new(msg)
			elsif resource.respond_to?(:cancellation_details)
				raise PaymentError.new(resource.cancellation_details.code.to_i)
			else
				return resource
			end
		end

		def create_moip_order(order)
			user = order.user
			moip_api(:order, :create, {
				own_id: order.number,
				amount: {
					currency: 'BRL'
				},
				items: [
					{
						product: order.campaign.title,
						quantity: order.tickets.length,
						price: order.campaign.raffle.price_cents
					}
				],
				customer: {
					own_id: user.id,
					fullname: user.name,
					email: user.email,
					birth_date: user.birthdate.strftime('%Y-%m-%d'),
					tax_document: {
						type: 'CPF',
						number: user.document_number
					},
					phone: {
						country_code: "55",
						area_code: user.phone_area_code.to_s,
						number: user.phone_number.to_s
					},
					shipping_address: {
						street: user.address.street,
						street_number: user.address.number,
						complement: user.address.complement,
						district: user.address.district,
						city: user.address.city,
						state: user.address.state,
						country: 'BRA',
						zip_code: user.address.zipcode 
					}
				},
				receivers: [
					{
						type: 'SECONDARY',
						fee_payor: false,
						moip_account: {
							id: order.campaign.publisher.moip_id
						},
						amount: {
							fixed: order.subtotal_cents
						}
					}
				]
			})
		end

		def create_order(user)
			order = build_order(user)
			order.save!
			m_order = create_moip_order(order)
			order.update!(moip_id: m_order.id)
			order
		end

		def create_moip_payment(order)
			params = @payment[:instrument] == 'credit_card' ? credit_card_json : boleto_json
			params.merge!({
				escrow: {
					description: 'custódia'
				}
			})
			moip_api(:payment, :create, order.moip_id, params)
		end

		def credit_card_json
			credit_card = @payment[:credit_card]
			birthdate = credit_card[:birthdate].to_date.strftime('%Y-%m-%d')
			phone_area_code = credit_card[:phone].gsub(/[^\d]/, '')[0, 2]
			phone_number = credit_card[:phone].gsub(/[^\d]/, '')[2..]
			{
				statement_descriptor: 'RifaIsso',
				delay_capture: true,
				funding_instrument: {
					method: 'CREDIT_CARD',
					credit_card: {
						hash: credit_card[:hash],
						store: false,
						holder: {
							fullname: credit_card[:fullname],
							birthdate: birthdate,
							phone: {
								country_code: "55",
								area_code: phone_area_code,
								number: phone_number
							},
							tax_document: {
								type: 'CPF',
								number: credit_card[:document_number]
							}
						}
					}
				}
			}
		end

		def boleto_json
			{
				funding_instrument: {
					method: 'BOLETO',
					boleto: {
						expiration_date: (Date.today + 1.day).strftime('%Y-%m-%d')
					}
				}
			}
		end

		def create_payment(order)
			m_payment = create_moip_payment(order)
			payment = order.build_payment(
				moip_id: m_payment.id,
				total_cents: m_payment.amount.total,
				fees_cents: m_payment.amount.fees,
				liquid_cents: m_payment.amount.liquid,
				refund_cents: m_payment.amount.refunds,
				instrument: @payment[:instrument],
				escrow_id: m_payment.escrows[0].id,
				status: :processing
			)
			if payment.credit_card_instrument?
				payment.assign_attributes({
					card_brand: m_payment.funding_instrument.credit_card.brand,
					card_number: m_payment.funding_instrument.credit_card.last4
				})
			else
				payment.assign_attributes({
					boleto_link: m_payment._links.pay_boleto.print_href,
					boleto_line_code: m_payment.funding_instrument.boleto.line_code
				})
			end
			payment.save!

			payment
		end

		def set_cancel_job
			campaign = Campaign.find(@campaign)
			if campaign.draw_date <= DateTime.now + 72.hours
				CancelWaitingOrderJob.set(wait: 30.minutes).perform_later(@order.id)
			end
		end

		class PaymentInstrumentNotAvailable < StandardError; end
		class InvalidCampaign < StandardError; end
		class TicketNotAvailable < StandardError; end
		class MoipError < StandardError; end
		class PaymentError < StandardError
			attr_reader :code
			def initialize(code)
				@code = code
				super
			end

			def error_code
				errors = {
					1 => :invalid_credit_card,
					3 => :not_authorized_credit_card,
					4 => :expired_credit_card,
					5 => :not_authorized_credit_card,
					6 => :duplicated_credit_card_transaction,
					12 => :not_authorized_credit_card
				}
				errors[code] || :payment_error
			end
		end
end
