class PerformCampaignDraw < ApplicationService
	attr_reader :campaign, :lottery_numbers

	def initialize(campaign, lottery_numbers)
		@campaign = campaign
		@lottery_numbers = lottery_numbers
	end

	def call
		tickets = CampaignDraw.new(campaign, lottery_numbers).init
		campaign.prizes.order(position: :asc).each_with_index do |prize, i|
			prize.ticket_id = tickets[i].id
			prize.save
		end
		campaign.waiting_receipt!
		Order.where(id: tickets.map(&:order_id)).each do |order|
			CampaignMailer.with(order: order).campaign_winner_email.deliver_later
		end
		PromoterMailer.with(campaign: campaign).campaign_drawn_email.deliver_later
	end
end
