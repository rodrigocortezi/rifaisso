require 'image_processing/mini_magick'

class ImageUploader < Shrine
	Attacher.derivatives do |original|
		magick = ImageProcessing::MiniMagick.source(original)

		{
			large: magick.resize_to_limit!(1000, 1000),
			medium: magick.resize_to_limit!(500, 500),
			small: magick.resize_to_limit!(150, 150)
		}
	end
end
