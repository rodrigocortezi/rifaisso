class CancelWaitingOrderJob < ActiveJob::Base
	queue_as :waiting_orders

	def perform(order_id)
		order = Order.find(order_id)
		if order.waiting?
			PaymentManagement.new(order: order).cancel_payment
			OrderMailer.with(order: order).payment_cancelled_email.deliver_later
		end
	end
end
