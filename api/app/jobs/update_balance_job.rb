class UpdateBalanceJob < ActiveJob::Base
	def perform(id)
			promoter = User.find(id)
			balances = Moip.api(promoter.moip_access_token).balances.show
			available_balance = [0, (balances.current.first.amount - balances.unavailable.first.amount)].max
			promoter.finance.update(balance_cents: available_balance)
	end
end
