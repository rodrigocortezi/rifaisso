class CampaignDrawJob < ActiveJob::Base

	def perform
		campaigns = Campaign.draw_today
		numbers = lottery_numbers

		unless numbers
			postpone_campaigns(campaigns)
		else
			ready_campaigns = campaigns.select { |c| c.goal_beaten? }
			ready_campaigns.each do |c|
				PerformCampaignDraw.call(c, numbers)
			end
			not_ready_campaigns = campaigns - ready_campaigns
			postpone_campaigns(not_ready_campaigns)
		end
	end

	private

		def next_draw_date
			next_wednesday = Date.current.next_occurring(:wednesday)
			next_saturday = Date.current.next_occurring(:saturday)
			[next_wednesday, next_saturday].min.change({ hour: 19 })
		end

		def postpone_campaigns campaigns
			campaigns.each do |c|
				c.update(draw_date: next_draw_date)
				PromoterMailer.with(campaign: c).campaign_postponed_email.deliver_later
				c.orders.paid.each do |o|
					CampaignMailer.with(order: o).campaign_postponed_email.deliver_later
				end
			end
		end

		def lottery_numbers
			retry_count = 3
			response = nil

			retry_count.times do
				response = HTTParty.get('http://www.loterias.caixa.gov.br/wps/portal/loterias/landing/federal/!ut/p/a1/jY6xDoIwFEU_qRcLrYyFJkDBlBhB6EJINKaJRQfj4NdbjYMOou9OLzk39xBDOmKm8WoP48WepvH4-A0bWFAhRwalVRFBlJrSRrYBwDzQeyDNRB7yCkC4XKCQSS55vAIK9l8fX07gV39LzCeS1alHtExiUB9EL2BO8QnMOPRekr9NqFZ4i2odbWgJaEoaZyfr7G2_I2fXdLC1uwMgq5ME/dl5/d5/L2dBISEvZ0FBIS9nQSEh/pw/Z7_HGK818G0KG4QF0QLDEU6PK2084/res/id=buscaResultado/c=cacheLevelPage/')
				break unless response.code != 200
			end

			unless response.code == 200
				return false
			end

			body = JSON.parse(response.body)
			numbers = body['dezenasSorteadasOrdemSorteio'].map(&:to_i)
			current = body['dataApuracao'] == Date.current.strftime('%d/%m/%Y')
			current && numbers
		end
end
