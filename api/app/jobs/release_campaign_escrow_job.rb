class ReleaseCampaignEscrowJob < ActiveJob::Base
	def perform(campaign_id)
		error = []
		@campaign = Campaign.find(campaign_id)
		@payments = @campaign.payments.where.not(escrow_id: nil)
		@payments.each do |payment|
			response = Moip.api.payment.release(payment.escrow_id)
			unless response.respond_to?(:status) && response.status == 'RELEASED'
				error.push(payment.id)
			end
		end
		@payments.where.not(id: error).update_all(escrow_id: nil)
	end
end
