class CancelSoldCampaignJob < ActiveJob::Base
	queue_as :campaigns

	def perform(campaign_id)
		campaign = Campaign.find(campaign_id)
		reverted = []
		ActiveRecord::Base.transaction do
			campaign.cancelled!
			campaign.orders.where(status: [:waiting, :paid]).each do |order|
				if order.waiting?
					PaymentManagement.new(order: order).cancel_payment
				else
					reverted.append(order)
					PaymentManagement.new(order: order).refund_payment
				end
				CampaignMailer.with(order: order).campaign_cancelled_email.deliver_later
			end
		end
		reverted.each do |order|
			Moip.api.refund.create(order.moip_id)
		end
	end
end
