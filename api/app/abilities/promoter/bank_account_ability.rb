class Promoter::BankAccountAbility
	include CanCan::Ability

	def initialize(user)
		user ||= User.new
		can :manage, BankAccount, finance: { user_id: user.id }
	end
end
