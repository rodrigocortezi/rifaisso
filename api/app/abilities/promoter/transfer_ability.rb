class Promoter::TransferAbility
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    can :create, Transfer, user_id: user.id
  end
end
