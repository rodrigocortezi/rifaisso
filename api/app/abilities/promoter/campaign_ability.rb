class Promoter::CampaignAbility
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    can :read, Campaign, publisher_id: user.id
    return unless user.has_complete_registration?
    can [:create, :destroy], Campaign, publisher_id: user.id
    can :update, Campaign do |campaign|
    	!campaign.sold?
    end
  end
end
