class Promoter::OrderAbility
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    can :read, Order, campaign: { publisher: user.id }
  end
end
