class Promoter::FinanceAbility
	include CanCan::Ability

	def initialize(user)
		user ||= User.new
		can :read, Finance, user_id: user.id
	end
end