class OrderAbility
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    can :create, Order
    return unless user.persisted?
    can [:read, :count], Order, user_id: user.id
  end
end
