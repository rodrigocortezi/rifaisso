class OrderMailer < ApplicationMailer
	def created_email
		@order = params[:order]
		user = @order.user
		mail(to: user.email, subject: 'Confirmação de participação')
	end

	def payment_confirmed_email
		@order = params[:order]
		user = @order.user
		mail(to: user.email, subject: 'Pagamento aprovado')
	end

	def payment_cancelled_email
		@order = params[:order]
		user = @order.user
		mail(to: user.email, subject: 'Pagamento rejeitado')
	end

	def payment_refunded_email
		@order = params[:order]
		user = @order.user
		mail(to: user.email, subject: 'Pagamento reembolsado')
	end

	def payment_reversed_email
		@order = params[:order]
		user = @order.user
		mail(to: user.email, subject: 'Pagamento estornado')
	end
end
