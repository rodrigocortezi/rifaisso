class PromoterMailer < ApplicationMailer
	def campaign_drawn_email
		@campaign = params[:campaign]
		publisher = @campaign.publisher
		mail(to: publisher.email, subject: 'O sorteio da sua campanha foi realizado.')
	end

	def campaign_postponed_email
		@campaign = params[:campaign]
		publisher = @campaign.publisher
		mail(to: publisher.email, subject: 'Adiamento automático da sua campanha.')
	end
end
