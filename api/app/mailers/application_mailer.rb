class ApplicationMailer < ActionMailer::Base
  default from: 'RifaIsso <noreply@rifaisso.com.br>'
  layout 'mailer'
end
