class ContactMailer < ApplicationMailer
  default from: 'Contato Website <contato-website@rifaisso.com.br>'
  default to: 'suporterifaisso@gmail.com'

  def support_email
    @contact = OpenStruct.new(params[:contact])
    mail(subject: @contact.subject, reply_to: @contact.email)
  end
end
