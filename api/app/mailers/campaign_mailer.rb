class CampaignMailer < ApplicationMailer
	def campaign_cancelled_email
		@order = params[:order]
		user = @order.user
		mail(to: user.email, subject: 'Campanha cancelada. Pagamento reembolsado.')
	end

	def campaign_winner_email
		@order = params[:order]
		user = @order.user
		mail(to: user.email, subject: 'Parabéns! Você ganhou a rifa!')
	end

	def campaign_postponed_email
		@order = params[:order]
		user = @order.user
		mail(to: user.email, subject: 'O sorteio da campanha que você participou foi adiado.')
	end
end