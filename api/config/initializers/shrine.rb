require 'shrine'

if Rails.env.development?
	require "shrine/storage/file_system"
	Shrine.storages = {
		store: Shrine::Storage::FileSystem.new("public", prefix: "uploads/")
	}
	Shrine.plugin :url_options, store: { host: 'http://localhost:3000' }
else
	require 'shrine/storage/s3'
	s3_options = {
		endpoint: 'https://s3.us-east-1.wasabisys.com',
	  bucket:            Rails.env.development? ? 'rifaisso-dev' : 'rifaisso',
	  access_key_id:     Rails.application.credentials.dig(:wasabi, :access_key_id),
	  secret_access_key: Rails.application.credentials.dig(:wasabi, :secret_access_key),
	  region:            'us-east-1'
	}
	 
	Shrine.storages = { 
	  store: Shrine::Storage::S3.new(s3_options)
	}
	Shrine.plugin :url_options, store: { expires_in: 3.days.to_i }
end
 
Shrine.plugin :activerecord 
Shrine.plugin :cached_attachment_data # for retaining the cached file across form redisplays 
Shrine.plugin :restore_cached_data # re-extract metadata when attaching a cached file 
Shrine.plugin :derivatives
Shrine.plugin :model, cache: false
