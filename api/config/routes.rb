Rails.application.routes.draw do
  use_doorkeeper scope: :auth do
    skip_controllers :authorizations, :applications,
      :authorized_applications
  end

  devise_for :users, skip: :all
  devise_scope :user do
    scope :auth, defaults: { format: :json } do
      post   '/signup',       to: 'user/registrations#create'
      put    '/account',      to: 'user/registrations#update'
      # delete '/account',      to: 'devise/registrations#destroy'
      put    '/password',     to: 'user/passwords#update'
      post   '/password',     to: 'user/passwords#create'
      get    '/confirmation', to: 'user/confirmations#show'
    end
  end

  resources :articles
  namespace :admin do
    get '/dashboard', to: 'dashboard#index'
    resources :articles
    get '/campaigns/count', to: 'campaigns#count'
    resources :campaigns, only: [:index, :show, :update, :destroy] do
      resources :orders, only: [:index]
      get '/orders/count', to: 'orders#count'
    end
    get '/orders/count', to: 'orders#count'
    resources :orders, only: [:index, :show]
    resources :transfers, only: [:index] do
      post '/approve', to: 'transfers#approve'
      post '/deny', to: 'transfers#deny'
    end
    get '/users/count', to: 'users#count'
    resources :users, only: [:index]
  end
  resources :campaigns, only: [:index, :show]
  namespace :promoter do
    get 'me', to: 'users#show'
    resources :campaigns
    resources :orders, only: [:index, :show]
    resource :finance, only: [:show]
    resource :bank_account, except: [:destroy]
    resources :transfers, only: [:create]
  end
  get '/orders/count', to: 'orders#count'
  resources :orders
  resource :address
  resources :transfers, except: [:update, :destroy]
  resource :bank_account
  resources :prizes, only: [:index]
  post '/prize_receipt/:id' => 'prize_receipts#create'
  post '/contacts', to: 'contacts#create'

  post '/webhooks' => 'moip/webhooks#create'

  if Rails.env.development?
    post '/webhooks' => 'moip/webhooks#create'
    require 'sidekiq/web'
    mount Sidekiq::Web => '/sidekiq'
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
