Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  config.hosts.clear

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports.
  config.consider_all_requests_local = true

  # Enable/disable caching. By default caching is disabled.
  # Run rails dev:cache to toggle caching.
  if Rails.root.join('tmp', 'caching-dev.txt').exist?
    config.cache_store = :redis_cache_store, { url: ENV.fetch('REDIS_URL_CACHING', 'redis://redis:6379/0') }
    config.public_file_server.headers = {
      'Cache-Control' => "public, max-age=#{2.days.to_i}"
    }
  else
    config.action_controller.perform_caching = false

    config.cache_store = :null_store
  end


  config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = { address: 'mailcatcher', port: 1025 }

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  config.action_mailer.perform_caching = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Highlight code that triggered database queries in logs.
  config.active_record.verbose_query_logs = true


  # Raises error for missing translations.
  # config.action_view.raise_on_missing_translations = true

  # Use an evented file watcher to asynchronously detect changes in source code,
  # routes, locales, etc. This feature depends on the listen gem.
  config.file_watcher = ActiveSupport::EventedFileUpdateChecker
  # config.after_initialize do
  #   response = Net::HTTP.get_response('ngrok', '/api/tunnels', 4040)
  #   body = JSON.parse(response.body)
  #   url = body['tunnels'][0]['public_url']
  #   notification = Moip.api.notifications.create({
  #       events: [
  #         'PAYMENT.PRE_AUTHORIZED',
  #         'PAYMENT.AUTHORIZED',
  #         'PAYMENT.CANCELLED',
  #         'PAYMENT.REFUNDED',
  #         'PAYMENT.REVERSED',
  #         'ESCROW.RELEASED',
  #         'TRANSFER.COMPLETED',
  #         'TRANSFER.FAILED'
  #       ],
  #       target: URI.join(url, 'webhooks'),
  #       media: 'WEBHOOK'
  #     },
  #     Moip.application_id
  #   )
  #   ENV['notification_token'] = notification.token
  # end
end
