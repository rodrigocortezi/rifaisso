FactoryBot.define do
	factory :user, aliases: [:publisher] do
		first_name { "Michael" }
		last_name { "Ross" }
		sequence(:email) { |n| "test-#{n.to_s.rjust(3, '0')}@sample.com" }
		password { '12345678' }
	end

	factory :address do
		user
		street { 'Av. Belisário Leite de Andrade Neto' }
		number { '354' }
		complement { '101' }
		district { 'Barra da Tijuca' }
		city { 'Rio de Janeiro' }
		state { 'Rio de Janeiro' }
		zipcode { '22621270' }
	end

	factory :bank_account do
		legal_name { 'Getúlio Vargas' }
		document_type { 'cpf' }
		document_number { '45501527294' }
		bank_code { '041' }
		agency_number { '0488' }
		agency_check_number { '00' }
		account_number { '02746' }
		account_check_number { '7' }
	end

	factory :finance do
		user
		bank_account
		balance_cents { 5000 }
	end

	factory :transfer do
		user
		bank_account
		total_cents { 1000 }
		fee_cents { 350 }
		status { :requested }
	end

	factory :campaign do
		transient do
			no_raffle { false }
		end
		publisher
		title { '2 Apple Watches Series 4' }
		purpose { 'Um propósito bacana.' }
		draw_date { DateTime.now + 3.days }
		goal { 0.5 }
		status { :active }
		after(:build) do |campaign, evaluator|
				campaign.build_raffle(price_cents: 500, quantity: 100) unless evaluator.no_raffle
		end
	end

	factory :order do
		user
		campaign
		sequence(:number) { |n| n.to_s.rjust(6, '0') }
		total_cents { 5000 }
		service_fee_cents { 500 }
		status { :waiting }
	end

	factory :payment do
		order
		total_cents { 5000 }
		fees_cents { 250 }
		liquid_cents { 4750 }
		refund_cents { 0 }
		status { :waiting }
		instrument { :boleto }
		factory :credit_card_payment do
			card_brand { 'mastercard' }
			card_number { '4633' }
			instrument { :credit_card }
		end
		factory :boleto_payment do
			card_brand { nil }
			card_number { nil }
		end
	end

	factory :raffle do
		transient do
			no_campaign { false }
		end
		price_cents { 100 }
		quantity { 100 }
		after(:build) do |raffle, evaluator|
			raffle.campaign = build :campaign, no_raffle: true unless evaluator.no_campaign
		end
	end

	factory :ticket do
		raffle
		order
		sequence(:number) { |n| n }
		status { :available }
		factory :purchased_ticket do
			status { :purchased }
		end
	end

	factory :prize do
		campaign
		ticket
		title { 'Apple Watch' }
		position { 0 }
	end
end
