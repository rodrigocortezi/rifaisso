require 'rails_helper'

RSpec.describe Raffle, type: :model do
	context 'without price_cents' do
		let(:raffle) { build :raffle, price_cents: nil }
		it { expect(raffle).not_to be_valid }
	end

	context 'with negative price_cents' do
		let(:raffle) { build :raffle, price_cents: -1 }
		it { expect(raffle).not_to be_valid }
	end

	context 'with zero price_cents' do
		let(:raffle) { build :raffle, price_cents: 0 }
		it { expect(raffle).not_to be_valid }
	end

	context 'without quantity' do
		let(:raffle) { build :raffle, quantity: nil }
		it { expect(raffle).not_to be_valid }
	end

	context 'with negative quantity' do
		let(:raffle) { build :raffle, quantity: -1 }
		it { expect(raffle).not_to be_valid }
	end

	context 'with zero quantity' do
		let(:raffle) { build :raffle, quantity: 0 }
		it { expect(raffle).not_to be_valid }
	end

	context 'without campaign' do
		let(:raffle) { build :raffle, no_campaign: true }
		it { expect(raffle).not_to be_valid }
	end
end
