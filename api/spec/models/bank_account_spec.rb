require 'rails_helper'

RSpec.describe BankAccount, type: :model do
	context 'without legal name' do
		let(:bank_account) { build :bank_account, legal_name: '' }
		it { expect(bank_account).not_to be_valid }
	end

	context 'without document type' do
		let(:bank_account) { build :bank_account, document_type: '' }
		it { expect(bank_account).not_to be_valid }
	end

	context 'without document number' do
		let(:bank_account) { build :bank_account, document_number: '' }
		it { expect(bank_account).not_to be_valid }
	end

	context 'with document number containing special characters' do
		let(:bank_account) { build :bank_account, document_number: '455.015.272-94' }
		it { expect(bank_account).not_to be_valid }
	end

	context 'without bank code' do
		let(:bank_account) { build :bank_account, bank_code: '' }
		it { expect(bank_account).not_to be_valid }
	end

	context 'without agency number' do
		let(:bank_account) { build :bank_account, agency_number: '' }
		it { expect(bank_account).not_to be_valid }
	end

	context 'without agency check number' do
		let(:bank_account) { build :bank_account, agency_check_number: '' }
		it { expect(bank_account).to be_valid }
	end

	context 'without account check number' do
		let(:bank_account) { build :bank_account, account_check_number: '' }
		it { expect(bank_account).not_to be_valid }
	end

	context 'without account number' do
		let(:bank_account) { build :bank_account, account_number: '' }
		it { expect(bank_account).not_to be_valid }
	end
end
