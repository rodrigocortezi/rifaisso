require 'rails_helper'

RSpec.describe Address, type: :model do
	context 'without street' do
		let(:address) { build :address, street: '' }
		it { expect(address).not_to be_valid }
	end

	context 'without number' do
		let(:address) { build :address, number: '' }
		it { expect(address).not_to be_valid }
	end

	context 'without complement' do
		let(:address) { build :address, complement: '' }
		it { expect(address).to be_valid }
	end

	context 'without district' do
		let(:address) { build :address, district: '' }
		it { expect(address).not_to be_valid }
	end

	context 'without city' do
		let(:address) { build :address, city: '' }
		it { expect(address).not_to be_valid }
	end

	context 'without state' do
		let(:address) { build :address, state: '' }
		it { expect(address).not_to be_valid }
	end

	context 'without zipcode' do
		let(:address) { build :address, zipcode: '' }
		it { expect(address).not_to be_valid }
	end

	context 'without associated user' do
		let(:address) { build :address, user: nil }
		it { expect(address).not_to be_valid }
	end
end
