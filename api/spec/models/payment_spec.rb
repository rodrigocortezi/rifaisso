require 'rails_helper'

RSpec.describe Payment, type: :model do
	context 'without total_cents' do
		let(:payment) { build :payment, total_cents: nil }
		it { expect(payment).not_to be_valid }
	end

	context 'with negative total_cents' do
		let(:payment) { build :payment, total_cents: -1 }
		it { expect(payment).not_to be_valid }
	end

	context 'with zero total_cents' do
		let(:payment) { build :payment, total_cents: 0 }
		it { expect(payment).not_to be_valid }
	end

	context 'without fees_cents' do
		let(:payment) { build :payment, fees_cents: nil }
		it { expect(payment).not_to be_valid }
	end

	context 'with negative fee_cents' do
		let(:payment) { build :payment, fees_cents: -1 }
		it { expect(payment).not_to be_valid }
	end

	context 'without refund_cents' do
		let(:payment) { build :payment, refund_cents: nil }
		it { expect(payment).to be_valid }
	end

	context 'without instrument' do
		let(:payment) { build :payment, instrument: nil }
		it { expect(payment).not_to be_valid }
	end

	context 'without status' do
		let(:payment) { build :payment, status: nil }
		it { expect(payment).not_to be_valid }
	end

	context 'by credit card without card_brand' do
		let(:payment) { build :credit_card_payment, card_brand: nil }
		it { expect(payment).not_to be_valid }
	end

	context 'by credit card without card_number' do
		let(:payment) { build :credit_card_payment, card_number: nil }
		it { expect(payment).not_to be_valid }
	end

	context 'by boleto without card_brand' do
		let(:payment) { build :boleto_payment, card_brand: nil }
		it { expect(payment).to be_valid }
	end

	context 'by boleto without card_number' do
		let(:payment) { build :boleto_payment, card_number: nil }
		it { expect(payment).to be_valid }
	end

	context 'by boleto with card_brand' do
		let(:payment) { build :boleto_payment, card_brand: 'mastercard' }
		it { expect(payment).not_to be_valid }
	end

	context 'by boleto with card_number' do
		let(:payment) { build :boleto_payment, card_number: '4633' }
		it { expect(payment).not_to be_valid }
	end
end
