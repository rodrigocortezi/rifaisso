require 'rails_helper'

RSpec.describe Order, type: :model do
	context 'without number' do
		let(:order) { build :order, number: '' }
		it { expect(order).not_to be_valid }
	end

	context 'without already existing number' do
		before { create :order, number: '01010101' }
		let(:order) { build :order, number: '01010101' }
		it { expect(order).not_to be_valid }
	end

	context 'without total_cents' do
		let(:order) { build :order, total_cents: nil }
		it { expect(order).not_to be_valid }
	end

	context 'with negative total_cents' do
		let(:order) { build :order, total_cents: -1 }
		it { expect(order).not_to be_valid }
	end

	context 'with zero total_cents' do
		let(:order) { build :order, total_cents: 0 }
		it { expect(order).not_to be_valid }
	end

	context 'without service_fee_cents' do
		let(:order) { build :order, service_fee_cents: nil }
		it { expect(order).not_to be_valid }
	end

	context 'with negative service_fee_cents' do
		let(:order) { build :order, service_fee_cents: -1 }
		it { expect(order).not_to be_valid }
	end

	context 'without status' do
		let(:order) { build :order, status: nil }
		it { expect(order).not_to be_valid }
	end

	context 'without user' do
		let(:order) { build :order, user: nil }
		it { expect(order).not_to be_valid }
	end

	context 'without campaign' do
		let(:order) { build :order, campaign: nil }
		it { expect(order).not_to be_valid }
	end
end
