require 'rails_helper'

RSpec.describe Campaign, type: :model do
	context 'without title' do
		let(:campaign) { build :campaign, title: '' }
		it { expect(campaign).not_to be_valid }
	end

	context 'without purpose' do
		let(:campaign) { build :campaign, purpose: '' }
		it { expect(campaign).not_to be_valid }
	end

	context 'without too long purpose' do
		let(:campaign) { build :campaign, purpose: 'a' * 1500 }
		it { expect(campaign).to be_valid }
	end

	context 'with too long purpose' do
		let(:campaign) { build :campaign, purpose: 'a' * 1501 }
		it { expect(campaign).not_to be_valid }
	end

	context 'without draw date' do
		let(:campaign) { build :campaign, draw_date: nil }
		it { expect(campaign).not_to be_valid }
	end

	context 'not persisted with past draw date' do
		let(:campaign) { build :campaign, draw_date: DateTime.now - 1.day }
		it { expect(campaign).not_to be_valid }
	end

	context 'persisted with past draw date' do
		it do
			campaign = create :campaign
			travel_to DateTime.now + 1.year
			expect(campaign).to be_valid
			travel_back
		end
	end

	context 'without goal' do
		let(:campaign) { build :campaign, goal: nil }
		it { expect(campaign).not_to be_valid }
	end

	context 'with negative goal' do
		let(:campaign) { build :campaign, goal: -1 }
		it { expect(campaign).not_to be_valid }
	end

	context 'with goal lower than 0.5' do
		let(:campaign) { build :campaign, goal: 0.3 }
		it { expect(campaign).not_to be_valid }
	end

	context 'with goal greater than 1' do
		let(:campaign) { build :campaign, goal: 1.1 }
		it { expect(campaign).not_to be_valid }
	end

	context 'without status' do
		let(:campaign) { build :campaign, status: nil }
		it { expect(campaign).not_to be_valid }
	end

	context 'without associated publisher' do
		let(:campaign) { build :campaign, publisher: nil }
		it { expect(campaign).not_to be_valid }
	end

	context 'without raffle' do
		let(:campaign) { build :campaign, no_raffle: true }
		it { expect(campaign).not_to be_valid }
	end
end
