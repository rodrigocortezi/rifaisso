require 'rails_helper'

RSpec.describe Prize, type: :model do
	context 'without title' do
		let(:prize) { build :prize, title: '' }
		it { expect(prize).not_to be_valid }
	end

	context 'without position' do
		let(:prize) { build :prize, position: nil }
		it { expect(prize).not_to be_valid }
	end

	context 'with negative position' do
		let(:prize) { build :prize, position: -1 }
		it { expect(prize).not_to be_valid }
	end

	context 'without campaign' do
		let(:prize) { build :prize, campaign: nil }
		it { expect(prize).not_to be_valid }
	end

	context 'without ticket' do
		let(:prize) { build :prize, ticket: nil }
		it { expect(prize).to be_valid }
	end

	context 'with ticket' do
		let(:prize) { build :prize }
		it { expect(prize).to be_valid }
	end
end
