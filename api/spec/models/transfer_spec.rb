require 'rails_helper'

RSpec.describe Transfer, type: :model do
	context 'without total_cents' do
		let(:transfer) { build :transfer, total_cents: nil }
		it { expect(transfer).not_to be_valid }
	end

	context 'with zero total_cents' do
		let(:transfer) { build :transfer, total_cents: 0 }
		it { expect(transfer).not_to be_valid }
	end

	context 'with negative total_cents' do
		let(:transfer) { build :transfer, total_cents: -1 }
		it { expect(transfer).not_to be_valid }
	end

	context 'without fee_cents' do
		let(:transfer) { build :transfer, fee_cents: nil }
		it { expect(transfer).not_to be_valid }
	end

	context 'with negative fee_cents' do
		let(:transfer) { build :transfer, fee_cents: -1 }
		it { expect(transfer).not_to be_valid }
	end

	context 'without associated bank_account' do
		let(:transfer) { build :transfer, bank_account: nil }
		it { expect(transfer).not_to be_valid }
	end

	context 'without associated user' do
		let(:transfer) { build :transfer, user: nil }
		it { expect(transfer).not_to be_valid }
	end

	context 'without status' do
		let(:transfer) { build :transfer, status: nil }
		it { expect(transfer).not_to be_valid }
	end
end
