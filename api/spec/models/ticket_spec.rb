require 'rails_helper'

RSpec.describe Ticket, type: :model do
	context 'without number' do
		let(:ticket) { build :ticket, number: nil }
		it { expect(ticket).not_to be_valid }
	end

	context 'without negative number' do
		let(:ticket) { build :ticket, number: -1 }
		it { expect(ticket).not_to be_valid }
	end

	context 'with existing number to same raffle' do
		let(:ticket) do
			existing = create :purchased_ticket, number: 1
			build :ticket, number: 1, raffle_id: existing.raffle.id
		end
		it { expect(ticket).not_to be_valid }
	end

	context 'with existing number to different raffle' do
		before { create :purchased_ticket, number: 1 }
		let(:ticket) { build :ticket, number: 1 }
		it { expect(ticket).to be_valid }
	end

	context 'without status' do
		let(:ticket) { build :ticket, status: nil }
		it { expect(ticket).not_to be_valid }
	end

	context 'without raffle' do
		let(:ticket) { build :ticket, raffle: nil }
		it { expect(ticket).not_to be_valid }
	end

	context 'without order' do
		let(:ticket) { build :ticket, order: nil }
		it { expect(ticket).not_to be_valid }
	end
end
