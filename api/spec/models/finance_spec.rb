require 'rails_helper'

RSpec.describe Finance, type: :model do
	context 'without balance_cents' do
		let(:finance) { build :finance, balance_cents: nil }
		it { expect(finance).not_to be_valid }
	end

	context 'with negavite balance_cents' do
		let(:finance) { build :finance, balance_cents: -1 }
		it { expect(finance).not_to be_valid }
	end

	context 'without bank_account' do
		let(:finance) { build :finance, bank_account: nil }
		it { expect(finance).to be_valid }
	end

	context 'with bank_account' do
		let(:finance) { build :finance }
		it { expect(finance).to be_valid }
	end

	context 'without user' do
		let(:finance) { build :finance, user: nil }
		it { expect(finance).not_to be_valid }
	end
end
