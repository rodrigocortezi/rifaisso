class AddMethodToPayment < ActiveRecord::Migration[6.0]
  def up
  	execute <<-SQL
  		CREATE TYPE payment_methods AS ENUM ('boleto', 'credit_card');
      ALTER TABLE payments ALTER COLUMN method TYPE payment_methods USING method::payment_methods;
  	SQL
  end

  def down
  	execute <<-SQL
  		DROP TYPE payment_methods;
  	SQL
  	change_column :payments, :method, :string
  end
end
