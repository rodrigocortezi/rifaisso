class PaymentAddBoletoColumns < ActiveRecord::Migration[6.0]
  def change
  	add_column :payments, :boleto_link, :string
  	add_column :payments, :boleto_line_code, :string
  end
end
