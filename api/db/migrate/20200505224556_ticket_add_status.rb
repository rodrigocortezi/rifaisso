class TicketAddStatus < ActiveRecord::Migration[6.0]
  def up
  	execute <<-SQL
  		ALTER TYPE ticket_statuses RENAME VALUE 'available' TO 'cancelled';
      ALTER TYPE ticket_statuses RENAME VALUE 'purchased' TO 'active';
  	SQL
  end
  def down
  	execute <<-SQL
  		ALTER TYPE ticket_statuses RENAME VALUE 'cancelled' TO 'available';
      ALTER TYPE ticket_statuses RENAME VALUE 'active' TO 'purchased';
  	SQL
  end
end
