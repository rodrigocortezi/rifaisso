class OrdersChangeServiceFee < ActiveRecord::Migration[6.0]
  def change
  	remove_column :orders, :service_fee_cents, :integer, null: false
  	add_column :orders, :service_fee, :decimal, null: false
  end
end
