class TransfersDropAdminApprovalColumn < ActiveRecord::Migration[6.0]
  def change
  	remove_column :transfers, :admin_approval, :boolean, default: false, null: false
  end
end
