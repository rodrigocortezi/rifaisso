class TransfersChangeStatus < ActiveRecord::Migration[6.0]
  def up
  	change_column :transfers, :status, :string
  	execute <<-SQL
  		DROP TYPE transfer_statuses;
  		CREATE TYPE transfer_statuses AS ENUM ('in_analysis', 'requested', 'completed', 'failed', 'not_approved');
      ALTER TABLE transfers ALTER COLUMN status TYPE transfer_statuses USING status::transfer_statuses;
  	SQL
  end
  def down
  	change_column :transfers, :status, :string
  	execute <<-SQL
  		DROP TYPE transfer_statuses;
  		CREATE TYPE transfer_statuses AS ENUM ('requested', 'completed', 'failed');
      ALTER TABLE transfers ALTER COLUMN status TYPE transfer_statuses USING status::transfer_statuses;
  	SQL
  end
end
