class TicketAddPartialUniqueIndex < ActiveRecord::Migration[6.0]
  def up
  	execute <<-SQL
  		CREATE UNIQUE INDEX unique_raffle_purchased_ticket
  		ON tickets (raffle_id, number)
  		WHERE status = 'purchased'
      OR status = 'reserved';
  	SQL
  end
  def down
  	execute <<-SQL
  		DROP INDEX unique_raffle_purchased_ticket;
  	SQL
  end
end
