class AddUniqueIndexToTicket < ActiveRecord::Migration[6.0]
  def change
  	add_index :tickets, [:number, :raffle_id], unique: true
  end
end
