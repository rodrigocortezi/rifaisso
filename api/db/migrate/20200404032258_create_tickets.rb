class CreateTickets < ActiveRecord::Migration[6.0]
  def change
    create_table :tickets do |t|
      t.integer :number, null: false
      t.string :status, null: false
      t.references :raffle, foreign_key: true, index: true, null: false
      t.references :order, foreign_key: true, index: true, null: false

      t.timestamps
    end
  end
end
