class CreatePayments < ActiveRecord::Migration[6.0]
  def change
    create_table :payments do |t|
      t.integer :total_cents, null: false
      t.integer :fees_cents, null: false
      t.integer :liquid_cents, null: false
      t.integer :refund_cents, default: 0, null: false
      t.string :card_brand
      t.string :card_number
      t.string :method, null: false
      t.string :status, null: false
      t.references :order, foreign_key: true, index: { unique: true }, null: false

      t.timestamps
    end
  end
end
