class TicketRemoveUniqueIndex < ActiveRecord::Migration[6.0]
  def change
  	remove_index :tickets, column: [:number, :raffle_id], unique: true
  end
end
