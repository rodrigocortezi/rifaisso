class CreateAddresses < ActiveRecord::Migration[6.0]
  def change
    create_table :addresses do |t|
      t.string :street, null: false
      t.string :number, null: false
      t.string :complement
      t.string :district, null: false
      t.string :city, null: false
      t.string :state, null: false
      t.string :zipcode, null: false
      t.references :user, foreign_key: true, index: { unique: true }, null: false

      t.timestamps
    end
  end
end
