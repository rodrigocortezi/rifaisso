class CreatePrizeReceipts < ActiveRecord::Migration[6.0]
  def change
    create_table :prize_receipts do |t|
			t.references :prize, foreign_key: true, index: { unique: true }, null: false
			t.references :user, foreign_key: true, index: true, null: false

      t.timestamps
    end
  end
end
