class CampaignNewAttributes < ActiveRecord::Migration[6.0]
  def up
  	execute <<-SQL
  		CREATE TYPE campaign_categories
  			AS ENUM ('vehicles','electronics', 'accessories', 'cash', 'travels', 'others');
  	SQL
		add_column :campaigns, :category, :campaign_categories, null: false, default: 'others'
		add_index :campaigns, :category
  	add_column :campaigns, :videos, :string, array: true
  end
  def down
  	remove_index :campaigns, :category
		remove_column :campaigns, :category
  	execute <<-SQL
  		DROP TYPE campaign_categories;
  	SQL
  	remove_column :campaigns, :videos
  end
end
