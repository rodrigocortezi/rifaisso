class AddStatusToOrders < ActiveRecord::Migration[6.0]
  def up
  	execute <<-SQL
  		CREATE TYPE order_statuses AS ENUM ('waiting', 'paid', 'not_paid', 'reverted');
      ALTER TABLE orders ALTER COLUMN status TYPE order_statuses USING status::order_statuses;
  	SQL
  end

  def down
  	execute <<-SQL
  		DROP TYPE order_statuses;
  	SQL
  	change_column :orders, :status, :string
  end
end
