class CreateBankAccounts < ActiveRecord::Migration[6.0]
  def change
    create_table :bank_accounts do |t|
      t.string :legal_name, null: false
      t.string :document_type, null: false
      t.string :document_number, null: false
      t.string :bank_code, null: false
      t.string :agency_number, null: false
      t.string :agency_check_number
      t.string :account_number, null: false
      t.string :account_check_number, null: false

      t.timestamps
    end
  end
end
