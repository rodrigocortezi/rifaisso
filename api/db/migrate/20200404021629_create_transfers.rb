class CreateTransfers < ActiveRecord::Migration[6.0]
  def change
    create_table :transfers do |t|
      t.integer :total_cents, null: false
      t.integer :fee_cents, default: 0, null: false
      t.string :status, null: false
      t.references :bank_account, foreign_key: true, index: true, null: false
      t.references :user, foreign_key: true, index: true, null: false

      t.timestamps
    end
  end
end
