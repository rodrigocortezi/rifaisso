class CampaignStatusNewValue < ActiveRecord::Migration[6.0]
  def up
  	change_column :campaigns, :status, :string
  	execute <<-SQL
  		DROP TYPE campaign_statuses;
  		CREATE TYPE campaign_statuses AS ENUM ('active', 'cancelled', 'waiting_receipt', 'finished');
      ALTER TABLE campaigns ALTER COLUMN status TYPE campaign_statuses USING status::campaign_statuses;
  	SQL
  end
  def down
  	change_column :campaigns, :status, :string
  	execute <<-SQL
  		DROP TYPE campaign_statuses;
  		CREATE TYPE campaign_statuses AS ENUM ('active', 'cancelled', 'finished');
      ALTER TABLE campaigns ALTER COLUMN status TYPE campaign_statuses USING status::campaign_statuses;
  	SQL
  end
end
