class AddStatusToCampaigns < ActiveRecord::Migration[6.0]
  def up
  	execute <<-SQL
  		CREATE TYPE campaign_statuses AS ENUM ('active', 'cancelled', 'finished');
      ALTER TABLE campaigns ALTER COLUMN status TYPE campaign_statuses USING status::campaign_statuses;
  	SQL
  end

  def down
  	execute <<-SQL
  		DROP TYPE campaign_statuses;
  	SQL
  	change_column :campaigns, :status, :string
  end
end
