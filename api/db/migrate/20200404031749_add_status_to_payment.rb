class AddStatusToPayment < ActiveRecord::Migration[6.0]
  def up
  	execute <<-SQL
  		CREATE TYPE payment_statuses AS
  			ENUM ('waiting', 'processing', 'settled', 'refused', 'refunded', 'chargedback');
      ALTER TABLE payments ALTER COLUMN status TYPE payment_statuses USING status::payment_statuses;
  	SQL
  end

  def down
  	execute <<-SQL
  		DROP TYPE payment_statuses;
  	SQL
  	change_column :payments, :status, :string
  end
end
