class ChangePaymentStatus < ActiveRecord::Migration[6.0]
  def up
  	change_column :payments, :status, :string
  	execute <<-SQL
  		DROP TYPE payment_statuses;
  		CREATE TYPE payment_statuses AS ENUM ('processing', 'settled', 'cancelled', 'refunded', 'reversed');
      ALTER TABLE payments ALTER COLUMN status TYPE payment_statuses USING status::payment_statuses;
  	SQL
  end
  def down
  	change_column :payments, :status, :string
  	execute <<-SQL
  		DROP TYPE payment_statuses;
  		CREATE TYPE payment_statuses AS ENUM ('waiting', 'processing', 'settled', 'refused', 'refunded', 'chargedback');
      ALTER TABLE payments ALTER COLUMN status TYPE payment_statuses USING status::payment_statuses;
  	SQL
  end
end
