class CreatePrizes < ActiveRecord::Migration[6.0]
  def change
    create_table :prizes do |t|
      t.string :title, null: false
      t.integer :position, null: false
      t.references :campaign, foreign_key: true, index: true, null: false
      t.references :ticket, foreign_key: true, index: true, null: true

      t.timestamps
    end
  end
end
