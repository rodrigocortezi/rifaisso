class ChangePurposeType < ActiveRecord::Migration[6.0]
  def up
  	change_column :campaigns, :purpose, :text
  end
  def down
  	change_column :campaigns, :purpose, :string
  end
end
