class CreateCampaigns < ActiveRecord::Migration[6.0]
  def change
    create_table :campaigns do |t|
      t.string :title, null: false
      t.string :purpose, null: false
      t.datetime :draw_date, null: false
      t.decimal :goal, null: false
      t.boolean :force_winner, default: false, null: false
      t.string :status, null: false
      t.references :publisher, foreign_key: { to_table: :users }, index: true, null: false

      t.timestamps
    end
  end
end
