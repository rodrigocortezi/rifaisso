class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
      t.string :number, null: false
      t.integer :total_cents, null: false
      t.integer :service_fee_cents, null: false
      t.string :status, null: false
      t.references :user, foreign_key: true, index: true, null: false
      t.references :campaign, foreign_key: true, index: true, null: false

      t.timestamps
    end
  end
end
