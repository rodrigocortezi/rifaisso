class MoipId < ActiveRecord::Migration[6.0]
  def change
  	add_column :users, :moip_id, :string
  	add_index :users, :moip_id
  	add_column :orders, :moip_id, :string
  	add_index :orders, :moip_id
  	add_column :payments, :moip_id, :string
  	add_index :payments, :moip_id
  	add_column :transfers, :moip_id, :string
  	add_index :transfers, :moip_id
  	add_column :bank_accounts, :moip_id, :string
  	add_index :bank_accounts, :moip_id
  end
end
