class AddStatusToTicket < ActiveRecord::Migration[6.0]
  def up
  	execute <<-SQL
  		CREATE TYPE ticket_statuses AS ENUM ('available', 'reserved', 'purchased');
      ALTER TABLE tickets ALTER COLUMN status TYPE ticket_statuses USING status::ticket_statuses;
  	SQL
  end

  def down
  	execute <<-SQL
  		DROP TYPE ticket_statuses;
  	SQL
  	change_column :tickets, :status, :string
  end
end
