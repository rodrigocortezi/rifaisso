class CreateRaffles < ActiveRecord::Migration[6.0]
  def change
    create_table :raffles do |t|
      t.integer :price_cents, null: false
      t.integer :quantity, null: false
      t.references :campaign, foreign_key: true, index: { unique: true }, null: false

      t.timestamps
    end
  end
end
