class RemoveForceWinner < ActiveRecord::Migration[6.0]
  def change
  	remove_column :campaigns, :force_winner, :boolean, default: false, null: false
  end
end
