class TransfersAddAdminApproval < ActiveRecord::Migration[6.0]
  def change
  	add_column :transfers, :admin_approval, :boolean, default: :false, null: false
  end
end
