class AddStatusToTransfer < ActiveRecord::Migration[6.0]
  def up
  	execute <<-SQL
  		CREATE TYPE transfer_statuses AS ENUM ('requested', 'completed', 'failed');
  		ALTER TABLE transfers ALTER COLUMN status TYPE transfer_statuses USING status::transfer_statuses;
  	SQL
  end

  def down
  	execute <<-SQL
  		DROP TYPE transfer_statuses;
  	SQL
  	change_column :transfers, :status, :string
  end
end
