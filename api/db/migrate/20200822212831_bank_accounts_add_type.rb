class BankAccountsAddType < ActiveRecord::Migration[6.0]
  def change
  	add_column :bank_accounts, :account_type, :string, null: false
  end
end
