class PrizeReceiptAdjustments < ActiveRecord::Migration[6.0]
  def change
    drop_table :prize_receipts do |t|
			t.references :prize, foreign_key: true, index: { unique: true }, null: false
			t.references :user, foreign_key: true, index: true, null: false
      t.timestamps
    end

   add_column :prizes, :received_at, :datetime, null: true 
  end
end
