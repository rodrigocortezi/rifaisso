class ChangeMethodEnumName < ActiveRecord::Migration[6.0]
  def change
  	rename_column :payments, :method, :instrument
  end
end
