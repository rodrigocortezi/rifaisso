class FinanceAddUnavailableCents < ActiveRecord::Migration[6.0]
  def change
  	add_column :finances, :unavailable_cents, :integer, default: 0
  end
end
