class PaymentsAddEscrowColumn < ActiveRecord::Migration[6.0]
  def change
  	add_column :payments, :escrow_id, :string
  end
end
