class CreateFinances < ActiveRecord::Migration[6.0]
  def change
    create_table :finances do |t|
      t.integer :balance_cents, default: 0, null: false
      t.references :bank_account, foreign_key: true, null: true
      t.references :user, foreign_key: true, index: true, null: false

      t.timestamps
    end
  end
end
