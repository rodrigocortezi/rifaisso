SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: campaign_categories; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.campaign_categories AS ENUM (
    'vehicles',
    'electronics',
    'accessories',
    'cash',
    'travels',
    'others'
);


--
-- Name: campaign_statuses; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.campaign_statuses AS ENUM (
    'active',
    'cancelled',
    'waiting_receipt',
    'finished'
);


--
-- Name: order_statuses; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.order_statuses AS ENUM (
    'waiting',
    'paid',
    'not_paid',
    'reverted'
);


--
-- Name: payment_methods; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.payment_methods AS ENUM (
    'boleto',
    'credit_card'
);


--
-- Name: payment_statuses; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.payment_statuses AS ENUM (
    'processing',
    'settled',
    'cancelled',
    'refunded',
    'reversed'
);


--
-- Name: ticket_statuses; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.ticket_statuses AS ENUM (
    'cancelled',
    'reserved',
    'active'
);


--
-- Name: transfer_statuses; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.transfer_statuses AS ENUM (
    'in_analysis',
    'requested',
    'completed',
    'failed',
    'not_approved'
);


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: addresses; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.addresses (
    id bigint NOT NULL,
    street character varying NOT NULL,
    number character varying NOT NULL,
    complement character varying,
    district character varying NOT NULL,
    city character varying NOT NULL,
    state character varying NOT NULL,
    zipcode character varying NOT NULL,
    user_id bigint NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: addresses_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.addresses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: addresses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.addresses_id_seq OWNED BY public.addresses.id;


--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: articles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.articles (
    id bigint NOT NULL,
    title character varying,
    text text,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: articles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.articles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: articles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.articles_id_seq OWNED BY public.articles.id;


--
-- Name: bank_accounts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bank_accounts (
    id bigint NOT NULL,
    legal_name character varying NOT NULL,
    document_type character varying NOT NULL,
    document_number character varying NOT NULL,
    bank_code character varying NOT NULL,
    agency_number character varying NOT NULL,
    agency_check_number character varying,
    account_number character varying NOT NULL,
    account_check_number character varying NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    moip_id character varying,
    account_type character varying NOT NULL
);


--
-- Name: bank_accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bank_accounts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bank_accounts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bank_accounts_id_seq OWNED BY public.bank_accounts.id;


--
-- Name: campaigns; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.campaigns (
    id bigint NOT NULL,
    title character varying NOT NULL,
    purpose text NOT NULL,
    draw_date timestamp without time zone NOT NULL,
    goal numeric NOT NULL,
    status public.campaign_statuses NOT NULL,
    publisher_id bigint NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    category public.campaign_categories DEFAULT 'others'::public.campaign_categories NOT NULL,
    videos character varying[]
);


--
-- Name: campaigns_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.campaigns_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: campaigns_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.campaigns_id_seq OWNED BY public.campaigns.id;


--
-- Name: finances; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.finances (
    id bigint NOT NULL,
    balance_cents integer DEFAULT 0 NOT NULL,
    bank_account_id bigint,
    user_id bigint NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    unavailable_cents integer DEFAULT 0
);


--
-- Name: finances_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.finances_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: finances_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.finances_id_seq OWNED BY public.finances.id;


--
-- Name: oauth_access_tokens; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.oauth_access_tokens (
    id bigint NOT NULL,
    resource_owner_id bigint,
    application_id integer,
    token character varying NOT NULL,
    refresh_token character varying,
    expires_in integer,
    revoked_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    scopes character varying,
    previous_refresh_token character varying DEFAULT ''::character varying NOT NULL
);


--
-- Name: oauth_access_tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.oauth_access_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: oauth_access_tokens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.oauth_access_tokens_id_seq OWNED BY public.oauth_access_tokens.id;


--
-- Name: orders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.orders (
    id bigint NOT NULL,
    number character varying NOT NULL,
    total_cents integer NOT NULL,
    status public.order_statuses NOT NULL,
    user_id bigint NOT NULL,
    campaign_id bigint NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    service_fee numeric NOT NULL,
    moip_id character varying
);


--
-- Name: orders_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.orders_id_seq OWNED BY public.orders.id;


--
-- Name: payments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.payments (
    id bigint NOT NULL,
    total_cents integer NOT NULL,
    fees_cents integer NOT NULL,
    liquid_cents integer NOT NULL,
    refund_cents integer DEFAULT 0 NOT NULL,
    card_brand character varying,
    card_number character varying,
    instrument public.payment_methods NOT NULL,
    status public.payment_statuses NOT NULL,
    order_id bigint NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    moip_id character varying,
    boleto_link character varying,
    boleto_line_code character varying,
    escrow_id character varying
);


--
-- Name: payments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.payments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: payments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.payments_id_seq OWNED BY public.payments.id;


--
-- Name: photos; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.photos (
    id bigint NOT NULL,
    imageable_type character varying NOT NULL,
    imageable_id bigint NOT NULL,
    image_data text NOT NULL
);


--
-- Name: photos_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.photos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: photos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.photos_id_seq OWNED BY public.photos.id;


--
-- Name: prizes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.prizes (
    id bigint NOT NULL,
    title character varying NOT NULL,
    "position" integer NOT NULL,
    campaign_id bigint NOT NULL,
    ticket_id bigint,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    received_at timestamp without time zone
);


--
-- Name: prizes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.prizes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: prizes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.prizes_id_seq OWNED BY public.prizes.id;


--
-- Name: raffles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.raffles (
    id bigint NOT NULL,
    price_cents integer NOT NULL,
    quantity integer NOT NULL,
    campaign_id bigint NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: raffles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.raffles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: raffles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.raffles_id_seq OWNED BY public.raffles.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


--
-- Name: tickets; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tickets (
    id bigint NOT NULL,
    number integer NOT NULL,
    status public.ticket_statuses NOT NULL,
    raffle_id bigint NOT NULL,
    order_id bigint NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: tickets_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tickets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tickets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tickets_id_seq OWNED BY public.tickets.id;


--
-- Name: transfers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.transfers (
    id bigint NOT NULL,
    total_cents integer NOT NULL,
    fee_cents integer DEFAULT 0 NOT NULL,
    status public.transfer_statuses NOT NULL,
    bank_account_id bigint NOT NULL,
    user_id bigint NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    moip_id character varying
);


--
-- Name: transfers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.transfers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: transfers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.transfers_id_seq OWNED BY public.transfers.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    confirmation_token character varying,
    confirmed_at timestamp without time zone,
    confirmation_sent_at timestamp without time zone,
    first_name character varying NOT NULL,
    last_name character varying NOT NULL,
    document_number character varying,
    birthdate timestamp without time zone,
    phone_area_code integer,
    phone_number integer,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    moip_id character varying,
    moip_access_token character varying,
    admin boolean DEFAULT false
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: addresses id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.addresses ALTER COLUMN id SET DEFAULT nextval('public.addresses_id_seq'::regclass);


--
-- Name: articles id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.articles ALTER COLUMN id SET DEFAULT nextval('public.articles_id_seq'::regclass);


--
-- Name: bank_accounts id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bank_accounts ALTER COLUMN id SET DEFAULT nextval('public.bank_accounts_id_seq'::regclass);


--
-- Name: campaigns id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.campaigns ALTER COLUMN id SET DEFAULT nextval('public.campaigns_id_seq'::regclass);


--
-- Name: finances id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.finances ALTER COLUMN id SET DEFAULT nextval('public.finances_id_seq'::regclass);


--
-- Name: oauth_access_tokens id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.oauth_access_tokens ALTER COLUMN id SET DEFAULT nextval('public.oauth_access_tokens_id_seq'::regclass);


--
-- Name: orders id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orders ALTER COLUMN id SET DEFAULT nextval('public.orders_id_seq'::regclass);


--
-- Name: payments id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.payments ALTER COLUMN id SET DEFAULT nextval('public.payments_id_seq'::regclass);


--
-- Name: photos id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.photos ALTER COLUMN id SET DEFAULT nextval('public.photos_id_seq'::regclass);


--
-- Name: prizes id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.prizes ALTER COLUMN id SET DEFAULT nextval('public.prizes_id_seq'::regclass);


--
-- Name: raffles id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.raffles ALTER COLUMN id SET DEFAULT nextval('public.raffles_id_seq'::regclass);


--
-- Name: tickets id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tickets ALTER COLUMN id SET DEFAULT nextval('public.tickets_id_seq'::regclass);


--
-- Name: transfers id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transfers ALTER COLUMN id SET DEFAULT nextval('public.transfers_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: addresses addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.addresses
    ADD CONSTRAINT addresses_pkey PRIMARY KEY (id);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: articles articles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.articles
    ADD CONSTRAINT articles_pkey PRIMARY KEY (id);


--
-- Name: bank_accounts bank_accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bank_accounts
    ADD CONSTRAINT bank_accounts_pkey PRIMARY KEY (id);


--
-- Name: campaigns campaigns_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.campaigns
    ADD CONSTRAINT campaigns_pkey PRIMARY KEY (id);


--
-- Name: finances finances_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.finances
    ADD CONSTRAINT finances_pkey PRIMARY KEY (id);


--
-- Name: oauth_access_tokens oauth_access_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.oauth_access_tokens
    ADD CONSTRAINT oauth_access_tokens_pkey PRIMARY KEY (id);


--
-- Name: orders orders_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);


--
-- Name: payments payments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.payments
    ADD CONSTRAINT payments_pkey PRIMARY KEY (id);


--
-- Name: photos photos_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.photos
    ADD CONSTRAINT photos_pkey PRIMARY KEY (id);


--
-- Name: prizes prizes_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.prizes
    ADD CONSTRAINT prizes_pkey PRIMARY KEY (id);


--
-- Name: raffles raffles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.raffles
    ADD CONSTRAINT raffles_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: tickets tickets_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tickets
    ADD CONSTRAINT tickets_pkey PRIMARY KEY (id);


--
-- Name: transfers transfers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transfers
    ADD CONSTRAINT transfers_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_addresses_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_addresses_on_user_id ON public.addresses USING btree (user_id);


--
-- Name: index_bank_accounts_on_moip_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_bank_accounts_on_moip_id ON public.bank_accounts USING btree (moip_id);


--
-- Name: index_campaigns_on_category; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_campaigns_on_category ON public.campaigns USING btree (category);


--
-- Name: index_campaigns_on_publisher_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_campaigns_on_publisher_id ON public.campaigns USING btree (publisher_id);


--
-- Name: index_finances_on_bank_account_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_finances_on_bank_account_id ON public.finances USING btree (bank_account_id);


--
-- Name: index_finances_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_finances_on_user_id ON public.finances USING btree (user_id);


--
-- Name: index_oauth_access_tokens_on_refresh_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_oauth_access_tokens_on_refresh_token ON public.oauth_access_tokens USING btree (refresh_token);


--
-- Name: index_oauth_access_tokens_on_resource_owner_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_oauth_access_tokens_on_resource_owner_id ON public.oauth_access_tokens USING btree (resource_owner_id);


--
-- Name: index_oauth_access_tokens_on_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_oauth_access_tokens_on_token ON public.oauth_access_tokens USING btree (token);


--
-- Name: index_orders_on_campaign_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_orders_on_campaign_id ON public.orders USING btree (campaign_id);


--
-- Name: index_orders_on_moip_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_orders_on_moip_id ON public.orders USING btree (moip_id);


--
-- Name: index_orders_on_number; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_orders_on_number ON public.orders USING btree (number);


--
-- Name: index_orders_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_orders_on_user_id ON public.orders USING btree (user_id);


--
-- Name: index_payments_on_moip_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_payments_on_moip_id ON public.payments USING btree (moip_id);


--
-- Name: index_payments_on_order_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_payments_on_order_id ON public.payments USING btree (order_id);


--
-- Name: index_prizes_on_campaign_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_prizes_on_campaign_id ON public.prizes USING btree (campaign_id);


--
-- Name: index_prizes_on_ticket_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_prizes_on_ticket_id ON public.prizes USING btree (ticket_id);


--
-- Name: index_raffles_on_campaign_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_raffles_on_campaign_id ON public.raffles USING btree (campaign_id);


--
-- Name: index_tickets_on_order_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_tickets_on_order_id ON public.tickets USING btree (order_id);


--
-- Name: index_tickets_on_raffle_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_tickets_on_raffle_id ON public.tickets USING btree (raffle_id);


--
-- Name: index_transfers_on_bank_account_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_transfers_on_bank_account_id ON public.transfers USING btree (bank_account_id);


--
-- Name: index_transfers_on_moip_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_transfers_on_moip_id ON public.transfers USING btree (moip_id);


--
-- Name: index_transfers_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_transfers_on_user_id ON public.transfers USING btree (user_id);


--
-- Name: index_users_on_confirmation_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_confirmation_token ON public.users USING btree (confirmation_token);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_email ON public.users USING btree (email);


--
-- Name: index_users_on_moip_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_on_moip_id ON public.users USING btree (moip_id);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON public.users USING btree (reset_password_token);


--
-- Name: unique_raffle_purchased_ticket; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_raffle_purchased_ticket ON public.tickets USING btree (raffle_id, number) WHERE ((status = 'active'::public.ticket_statuses) OR (status = 'reserved'::public.ticket_statuses));


--
-- Name: transfers fk_rails_09d60ae02b; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transfers
    ADD CONSTRAINT fk_rails_09d60ae02b FOREIGN KEY (bank_account_id) REFERENCES public.bank_accounts(id);


--
-- Name: campaigns fk_rails_156296bd0b; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.campaigns
    ADD CONSTRAINT fk_rails_156296bd0b FOREIGN KEY (publisher_id) REFERENCES public.users(id);


--
-- Name: tickets fk_rails_16c99d60c5; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tickets
    ADD CONSTRAINT fk_rails_16c99d60c5 FOREIGN KEY (raffle_id) REFERENCES public.raffles(id);


--
-- Name: transfers fk_rails_344b52b7fd; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transfers
    ADD CONSTRAINT fk_rails_344b52b7fd FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: addresses fk_rails_48c9e0c5a2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.addresses
    ADD CONSTRAINT fk_rails_48c9e0c5a2 FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: orders fk_rails_5cdf762b9e; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT fk_rails_5cdf762b9e FOREIGN KEY (campaign_id) REFERENCES public.campaigns(id);


--
-- Name: payments fk_rails_6af949464b; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.payments
    ADD CONSTRAINT fk_rails_6af949464b FOREIGN KEY (order_id) REFERENCES public.orders(id);


--
-- Name: prizes fk_rails_88592b5736; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.prizes
    ADD CONSTRAINT fk_rails_88592b5736 FOREIGN KEY (ticket_id) REFERENCES public.tickets(id);


--
-- Name: finances fk_rails_9740b85de3; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.finances
    ADD CONSTRAINT fk_rails_9740b85de3 FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: prizes fk_rails_a8f91877d2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.prizes
    ADD CONSTRAINT fk_rails_a8f91877d2 FOREIGN KEY (campaign_id) REFERENCES public.campaigns(id);


--
-- Name: finances fk_rails_bf92cd3f5b; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.finances
    ADD CONSTRAINT fk_rails_bf92cd3f5b FOREIGN KEY (bank_account_id) REFERENCES public.bank_accounts(id);


--
-- Name: tickets fk_rails_c6410ba81d; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tickets
    ADD CONSTRAINT fk_rails_c6410ba81d FOREIGN KEY (order_id) REFERENCES public.orders(id);


--
-- Name: raffles fk_rails_e508c801f2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.raffles
    ADD CONSTRAINT fk_rails_e508c801f2 FOREIGN KEY (campaign_id) REFERENCES public.campaigns(id);


--
-- Name: oauth_access_tokens fk_rails_ee63f25419; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.oauth_access_tokens
    ADD CONSTRAINT fk_rails_ee63f25419 FOREIGN KEY (resource_owner_id) REFERENCES public.users(id);


--
-- Name: orders fk_rails_f868b47f6a; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT fk_rails_f868b47f6a FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO "schema_migrations" (version) VALUES
('20200402121626'),
('20200402121628'),
('20200403223225'),
('20200403235951'),
('20200404021629'),
('20200404021748'),
('20200404022517'),
('20200404023954'),
('20200404024823'),
('20200404030203'),
('20200404030810'),
('20200404031125'),
('20200404031523'),
('20200404031749'),
('20200404031803'),
('20200404032258'),
('20200404032949'),
('20200404033125'),
('20200404033355'),
('20200404033632'),
('20200409184139'),
('20200409235247'),
('20200410012126'),
('20200420201322'),
('20200430183902'),
('20200505185617'),
('20200505192131'),
('20200505201801'),
('20200505224556'),
('20200506160320'),
('20200518201705'),
('20200519154902'),
('20200519232439'),
('20200730173437'),
('20200808164352'),
('20200811211753'),
('20200811233521'),
('20200820195549'),
('20200822212831'),
('20200826231009'),
('20200928210052'),
('20201022213528'),
('20210106161105'),
('20210108141021'),
('20210108141857'),
('20210108165825');


