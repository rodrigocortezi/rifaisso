module States
	class Callback
		attr_reader :from, :to, :callback

		def initialize(from: nil, to: nil, callback: nil)
			unless callback.respond_to?(:call)
				raise InvalidCallbackError, 'No callback passed'
			end

			@from = from
			@to = to
			@callback = callback
		end

		def call(*args)
			callback.call(*args)
		end

		def applies_to?(from: nil, to: nil)
			matches(from, to)
		end

		private

			def matches(from, to)
				matches_all_transactions ||
					matches_to_state(from, to) ||
					matches_from_state(from, to) ||
					matches_both_states(from, to)
			end

			def matches_all_transactions
				from.nil? && to.empty?
			end

			def matches_to_state(from, to)
				((from.nil? || self.from.nil?) && self.to.include?(to))
			end

			def matches_from_state(from, to)
				((to.nil? || self.to.empty?) && self.from == from)
			end

			def matches_both_states(from, to)
				from == self.from && self.to.include?(to)
			end
	end
end
