module States
  class InvalidStateError < StandardError; end
  class InvalidTransitionError < StandardError; end
  class TransitionFailedError < StandardError
  	attr_reader :from, :to

  	def initialize(from, to)
  		@from = from
  		@to = to
  		super(_message)
  	end

  	private

  		def _message
  			"Cannot transition from '#{from}' to '#{to}'"
  		end
  end

	def self.included(base)
		base.extend(ClassMethods)
	end

	module ClassMethods
		def states
			@states ||= []	
		end

		def state(name)
			name = name.to_s
			states << name
		end

		def callbacks
			@callbacks ||= { before: [], after: [] }
		end

		def transition(from: nil, to: nil)
			from = to_s_or_nil(from)
			to = array_to_s_or_nil(to)

			raise InvalidStateError, 'No states provided.' if to.empty?

			successors[from] ||= []

			([from] + to).each { |state| validate_state(state) }

			successors[from] += to
		end

		def successors
			@successors ||= {}
		end

		def before_transition(from: nil, to: nil, &block)
			add_callback(callback_type: :before, from: from, to: to, &block)
		end

		def after_transition(from: nil, to: nil, &block)
			add_callback(callback_type: :after, from: from, to: to, &block)
		end

		private

			def to_s_or_nil(input)
				input.nil? ? input : input.to_s
			end

			def array_to_s_or_nil(input)
				Array(input).map { |item| to_s_or_nil(item) }
			end

			def validate_state(state)
        unless states.include?(state.to_s)
          raise InvalidStateError, "Invalid state '#{state}'"
        end
      end

      def add_callback(callback_type: nil, from: nil, to: nil, &block)
      	validate_callback_type(callback_type)

      	from = to_s_or_nil(from)
      	to = array_to_s_or_nil(to)

      	validate_callback_condition(from, to)

      	callbacks[callback_type] << Callback.new(from: from, to: to, callback: block)
      end

      def validate_callback_type(callback_type)
      	raise ArgumentError, 'missing keyword: callback_type' if callback_type.nil?
      end

      def validate_callback_condition(from, to)
      	([from] + to).compact.each { |state| validate_state(state) }
      	return if from.nil? && to.empty?

      	validate_not_from_terminal_state(from)
      	to.each { |state| validate_not_to_initial_state(state) }

      	return if from.nil? || to.empty?

      	to.each { |state| validate_from_and_to_state(from, state) }
			end

			def validate_not_from_terminal_state(from)
				unless from.nil? || successors.key?(from)
					raise InvalidTransitionError,
						"Cannot transition away from terminal state '#{from}'"
				end
			end

			def validate_not_to_initial_state(to)
				unless to.nil? || successors.values.flatten.include?(to)
					raise InvalidTransitionError,
					 "Cannot transition to initial state '#{to}'"
				end
			end

			def validate_from_and_to_state(from, to)
				unless successors.fetch(from, []).include?(to)
					raise InvalidTransitionError,
						"Cannot transition from '#{from}' to '#{to}'"
				end
			end
	end

	def initialize(object)
		@object = object
	end

	def current_state
		@object.status.to_s
	end

	def transition_to!(new_state)
		initial_state = current_state
		new_state = new_state.to_s

		validate_transition(from: initial_state, to: new_state)

		execute(:before, initial_state, new_state)
		@object.update!(status: new_state)
		execute(:after, initial_state, new_state)

		true
	end

	def transition_to(new_state)
		transition_to!(new_state)
	rescue TransitionFailedError
		false
	end

	private

		def execute(phase, initial_state, new_state)
			callbacks = callbacks_for(phase, from: initial_state, to: new_state)
			callbacks.each { |cb| cb.call(@object) }
		end

		def callbacks_for(phase, from: nil, to: nil)
			callbacks = self.class.callbacks[phase]
			callbacks.select { |cb| cb.applies_to?(from: from, to: to) }
		end

		def validate_transition(options = { from: nil, to: nil })
			from = to_s_or_nil(options[:from])
			to = to_s_or_nil(options[:to])

			successors = self.class.successors[from] || []
			raise TransitionFailedError.new(from, to) unless successors.include?(to)
		end

		def to_s_or_nil(input)
			input.nil? ? input : input.to_s
		end
end
