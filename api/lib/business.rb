module Business
	path = 'lib/business_rules.yml'
	rules = YAML.load_file(path)

	rules.each do |key, value|
		Business.const_set(key.camelize, value)
	end
end