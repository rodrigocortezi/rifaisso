module Moip
  class Webhooks
    attr_accessor :model, :event, :date, :env, :resource, :events

    class << self
      def build(json)
        object = new
        object.model    = get_model(json['event'])
        object.event    = get_event(json['event'])
        object.events   = {}
        object.date     = json['date']
        object.env      = json['env']
        object.resource = get_resource json['resource'], object.model

        object
      end

      def listen(params, &block)
        hook = build(params)

        resource_id = hook.resource.id
        event = hook.event
        log = Rails.cache.read(resource_id)
        if log.nil?
          Rails.cache.write(resource_id, Set[event], expires_in: 1.minute)
        elsif log.include?(event)
          return
        else
          Rails.cache.write(resource_id, log.add(event), expires_in: 1.minute)
        end

        yield hook
        hook.run
      end

      private

	      def get_model(event)
	        event.split(".")[0].downcase.to_sym
	      end

	      def get_event(event)
	        event.split(".")[1].downcase.to_sym
	      end

	      def get_resource(resource, model)
	        RecursiveOpenStruct.new resource[model.to_s]
	      end
    end

    def on(model, on_events, &block)
      unless events[model]
        events[model] = {}
      end

      (on_events.is_a?(Array) ? on_events : [on_events]).each do |event|
        events[model][event] = block
      end
    end

    def missing(&block)
      events[:missing] = block
    end

    def run
      return events[model][event].call(resource) if (events[model] && events[model][event])
      events[:missing].call(model, event) if events[:missing]
    end
  end
end
