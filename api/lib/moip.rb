module Moip
	class << self
		attr_reader :token

		def api(token=nil)
			@token = token || get_token
			moip_api
		end

		def notification_token
			ENV['notification_token'] ||
			Rails.application.credentials.dig(:moip, env, :notification_token)
		end

		def application_id
			Rails.application.credentials.dig(:moip, env, :app_id)
		end

		private

			def get_token
				Rails.application.credentials.dig(:moip, env, :access_token)
			end

			def env
				envs = { 'development' => :sandbox, 'production' => :production }
				envs[Rails.env]
			end

			def moip_api
				Moip2::Api.new(moip_client)
			end

			def moip_client
				# TODO: modo produção precisa ser ativado
				Moip2::Client.new(:sandbox, moip_auth)
			end

			def moip_auth
				Moip2::Auth::OAuth.new(token)
			end
	end
end
