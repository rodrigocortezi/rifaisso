desc 'Uploads and attaches existing legacy attached images to campaigns.'
task upload_legacy_images: [:environment] do
	Campaign.all.each do |campaign|
		campaign.images.each do |image|
			image.open do |file|
				campaign.photos.create(image: file)
			end
		end
	end
end