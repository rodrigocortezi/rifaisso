namespace :moip do
	desc 'Generates notification preferences.'
	task :generate_notifications, [:url] => :environment do |task, args|
		notification = Moip.api.notifications.create({
				events: [
					'PAYMENT.PRE_AUTHORIZED',
					'PAYMENT.AUTHORIZED',
					'PAYMENT.CANCELLED',
					'PAYMENT.REFUNDED',
					'PAYMENT.REVERSED'
				],
				target: args[:url],
				media: 'WEBHOOK'
			},
			Moip.application_id
		)
		puts 'Notificações criadas.'
		puts "token: #{notification.token}"
	end
end
